#include <chrono>
#include <cmath>
#include <iostream>
#include <numbers>

#include "Heat.hpp"

// - Setup -----------------------------------------------------------------------------------------
using Float = double;

constexpr auto a = static_cast<Float>(1.0);

constexpr size_t n   = 100;
constexpr auto x_min = static_cast<Float>(0.0);
constexpr auto x_max = static_cast<Float>(10.0);
constexpr auto dx    = (x_max - x_min) / static_cast<Float>(n - 1);

constexpr auto t_end = static_cast<Float>(0.5);
constexpr auto dt    = static_cast<Float>(0.5) * dx * dx * dx / a;

// - One possible analytical solution for Neumann boundary conditions with derivative zero ---------
[[nodiscard]] constexpr auto analytical_solution(Float x, Float t) noexcept -> Float {
  constexpr std::array z = {static_cast<Float>(1.0), static_cast<Float>(3.0)};

  auto res = static_cast<Float>(0);
  for (auto zi : z) {
    res += static_cast<Float>(
        std::cos((zi * std::numbers::pi_v<Float>) / (x_max - x_min) * x) *
        std::exp(-a * std::pow((zi * std::numbers::pi_v<Float>) / (x_max - x_min), 2) * t));
  }
  return res;
}

// -------------------------------------------------------------------------------------------------
template <typename LaplaceOp, typename Integrator, typename Boundary>
[[nodiscard]] auto run_solver(LaplaceOp laplace,
                              Integrator integrator,
                              Boundary boundary) noexcept -> Heat::Grid::OneD<Float, Float> {
  const auto grid_func = [&](const Heat::Grid::Idx<1>& idx) {
    return x_min + static_cast<Float>(idx[0]) * dx;
  };

  auto grid = Heat::Grid::OneD<Float, Float>{
      n, grid_func, [](const auto x) { return analytical_solution(x, static_cast<Float>(0)); }};

  auto rhs = Heat::Rhs::HeatEq{a, laplace};

  const auto t_begin = std::chrono::high_resolution_clock::now();
  const auto solver =
      Heat::Solver<Float, Float, decltype(grid), decltype(rhs), Boundary, Integrator>{
          std::move(rhs), std::move(integrator), std::move(boundary), std::move(grid)};

  const auto sol = solver.solve(t_end, dt);
  const auto t_dur =
      std::chrono::duration<double>(std::chrono::high_resolution_clock::now() - t_begin);
  HEAT_INFO("Solver took " << t_dur.count() << "s");

  return sol;
}

// -------------------------------------------------------------------------------------------------
template <typename LaplaceOp, typename Integrator, typename Boundary>
void run_test(const Eigen::VectorX<Float>& analytical_sol,
              LaplaceOp laplace,
              Integrator integrator,
              Boundary boundary) noexcept {
  assert(analytical_sol.rows() == static_cast<Eigen::Index>(n));

  HEAT_INFO("laplace    = " << LaplaceOp::name());
  HEAT_INFO("integrator = " << Integrator::name());
  HEAT_INFO("boundary   = " << Boundary::name());

  const auto numerical_solution =
      run_solver(std::move(laplace), std::move(integrator), std::move(boundary));

  const auto diff = (analytical_sol - numerical_solution.values()).eval();
  const auto rel_diff =
      (analytical_sol - numerical_solution.values()).cwiseQuotient(analytical_sol).eval();

  HEAT_INFO("MSE = " << diff.squaredNorm() / static_cast<Float>(n));
  HEAT_INFO("RSE = " << diff.squaredNorm() /
                            (static_cast<Float>(n) * analytical_sol.squaredNorm()));

  // HEAT_INFO("1-norm error:   " << diff.template lpNorm<1>());
  // HEAT_INFO("2-norm error:   " << diff.template lpNorm<2>());
  // HEAT_INFO("inf-norm error: " << diff.template lpNorm<Eigen::Infinity>());

  // HEAT_INFO("1-norm rel. error:   " << rel_diff.template lpNorm<1>());
  // HEAT_INFO("2-norm rel. error:   " << rel_diff.template lpNorm<2>());
  // HEAT_INFO("inf-norm rel. error: " << rel_diff.template lpNorm<Eigen::Infinity>());
  std::cout << "------------------------------------------------------------\n";
}

auto main() -> int {

  Eigen::VectorX<Float> analytical_sol(n);
  for (Eigen::Index i = 0; i < static_cast<Eigen::Index>(n); ++i) {
    analytical_sol(i) = analytical_solution(x_min + static_cast<Float>(i) * dx, t_end);
  }

  run_test(analytical_sol,
           Heat::Laplace::CentralFD{},
           Heat::TimeIntegration::ExplicitEuler{},
           Heat::Boundary::Neumann<Float, 1>{0.0, 0.0});
  run_test(analytical_sol,
           Heat::Laplace::CentralFD{},
           Heat::TimeIntegration::ImplicitEuler{},
           Heat::Boundary::Neumann<Float, 1>{0.0, 0.0});
  run_test(analytical_sol,
           Heat::Laplace::CentralFD{},
           Heat::TimeIntegration::RK4{},
           Heat::Boundary::Neumann<Float, 1>{0.0, 0.0});
  run_test(analytical_sol,
           Heat::Laplace::CentralFD{},
           Heat::TimeIntegration::RK45{1e-12},
           Heat::Boundary::Neumann<Float, 1>{0.0, 0.0});

  std::cout << "================================================================================\n";

  run_test(analytical_sol,
           Heat::Laplace::CentralFD{},
           Heat::TimeIntegration::ExplicitEuler{},
           Heat::Boundary::Neumann<Float, 2>{0.0, 0.0});
  run_test(analytical_sol,
           Heat::Laplace::CentralFD{},
           Heat::TimeIntegration::ImplicitEuler{},
           Heat::Boundary::Neumann<Float, 2>{0.0, 0.0});
  run_test(analytical_sol,
           Heat::Laplace::CentralFD{},
           Heat::TimeIntegration::RK4{},
           Heat::Boundary::Neumann<Float, 2>{0.0, 0.0});
  run_test(analytical_sol,
           Heat::Laplace::CentralFD{},
           Heat::TimeIntegration::RK45{1e-12},
           Heat::Boundary::Neumann<Float, 2>{0.0, 0.0});

  std::cout << "================================================================================\n";

  run_test(analytical_sol,
           Heat::Laplace::CentralFD4{},
           Heat::TimeIntegration::ExplicitEuler{},
           Heat::Boundary::Neumann<Float, 2>{0.0, 0.0});
  run_test(analytical_sol,
           Heat::Laplace::CentralFD4{},
           Heat::TimeIntegration::ImplicitEuler{},
           Heat::Boundary::Neumann<Float, 2>{0.0, 0.0});
  run_test(analytical_sol,
           Heat::Laplace::CentralFD4{},
           Heat::TimeIntegration::RK4{},
           Heat::Boundary::Neumann<Float, 2>{0.0, 0.0});
  run_test(analytical_sol,
           Heat::Laplace::CentralFD4{},
           Heat::TimeIntegration::RK45{1e-12},
           Heat::Boundary::Neumann<Float, 2>{0.0, 0.0});
}
