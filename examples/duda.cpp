#include <filesystem>
#include <iostream>
#include <tuple>
namespace fs = std::filesystem;

#include <AD/ad.hpp>

#include "BaselineSolver.hpp"
#include "EigenToNpy.hpp"

// - Calculate du/da using finite differences ------------------------------------------------------
template <typename Float>
[[nodiscard]] constexpr auto duda_fd(const Eigen::VectorX<Float>& u0,
                                     Float a,
                                     Float tend,
                                     Float dx,
                                     Float dt,
                                     Heat::SolverMode mode) noexcept
    -> std::optional<std::tuple<Eigen::VectorX<Float>, Eigen::VectorX<Float>>> {

  const auto opt_u = Heat::solve(u0, a, tend, dx, dt, mode);
  if (!opt_u.has_value()) {
    return std::nullopt;
  }

  const auto da         = std::sqrt(std::numeric_limits<Float>::epsilon());
  const auto opt_u_plus = Heat::solve(u0, a + da, tend, dx, dt, mode);
  if (!opt_u_plus.has_value()) {
    return std::nullopt;
  }

  Eigen::VectorX<Float> grad = (*opt_u_plus - *opt_u) / da;

  return std::tuple{*opt_u, grad};
}

// - Calculate du/da using algorithmic differentiation ---------------------------------------------
template <typename Float>
[[nodiscard]] constexpr auto duda_ad(const Eigen::VectorX<Float>& u0,
                                     Float a,
                                     Float tend,
                                     Float dx,
                                     Float dt,
                                     Heat::SolverMode mode) noexcept
    -> std::optional<std::tuple<Eigen::VectorX<Float>, Eigen::VectorX<Float>>> {
  using ADType = ad::gt1s<Float>::type;

  Eigen::VectorX<ADType> ad_u0 = u0;

  const ADType ad_a    = a;
  ad::derivative(ad_a) = 1.0;

  const auto opt_u = Heat::solve(ad_u0, ad_a, tend, dx, dt, mode);
  if (!opt_u.has_value()) {
    return std::nullopt;
  }

  Eigen::VectorX<Float> u(u0.rows());
  std::transform(std::cbegin(*opt_u), std::cend(*opt_u), std::begin(u), [](const ADType& ad_ui) {
    return ad::value(ad_ui);
  });

  Eigen::VectorX<Float> grad(u0.rows());
  std::transform(std::cbegin(*opt_u), std::cend(*opt_u), std::begin(grad), [](const ADType& ad_ui) {
    return ad::derivative(ad_ui);
  });

  return std::tuple{u, grad};
}

// -------------------------------------------------------------------------------------------------
auto main(int argc, char** argv) -> int {
  if (argc < 3) {
    std::cerr << "Usage: " << argv[0] << " <x-grid size> <solution mode>\n";  // NOLINT
    std::cerr << "  solution mode: 0 - ExplicitMatrixFree\n";
    std::cerr << "                 1 - Explicit\n";
    std::cerr << "                 2 - Implicit\n";
    return 1;
  }

  // - Setup -------------------------------------------------------------------
  using Float = double;

  const int64_t n = std::atol(argv[1]);  // NOLINT
  if (n <= 0) {
    std::cerr << "x-grid size must be an integer greater than zero, but is `" << argv[1]  // NOLINT
              << "`.\n";
    return 1;
  }

  Heat::SolverMode mode;
  switch (*(argv[2])) {  // NOLINT
    case '0':
      mode = Heat::SolverMode::ExplicitMatrixFree;
      break;
    case '1':
      mode = Heat::SolverMode::Explicit;
      break;
    case '2':
      mode = Heat::SolverMode::Implicit;
      break;
    default:
      std::cerr << "Invaild solution mode `" << argv[2] << "` has to be 0, 1, or 2.\n";  // NOLINT
      return 1;
  }

  Eigen::VectorX<Float> u0 = Eigen::VectorX<Float>::Zero(n);
  u0(n - 1)                = 100.0;

  const Float a = 0.5;

  const Float tend = 0.5;

  const Float dx = 1.0 / static_cast<Float>(n);

  Float dt;
  switch (mode) {
    case Heat::SolverMode::ExplicitMatrixFree:
    case Heat::SolverMode::Explicit:
      dt = 0.5 * dx * dx / a;
      break;
    default:
      dt = dx;
      break;
  }

  HEAT_INFO("Calcutate du/da with finite differences and with algorithmic differentiation:");
  HEAT_INFO("a  = " << a);
  HEAT_INFO("dx = " << dx);
  HEAT_INFO("dt = " << dt);
  HEAT_INFO("mode = " << mode);
  std::cout << "\n------------------------------------------------------------\n\n";

  // - Finite differences ------------------------------------------------------
  const auto t_fd_begin = std::chrono::high_resolution_clock::now();
  const auto opt_fd     = duda_fd(u0, a, tend, dx, dt, mode);
  const auto t_fd_dur =
      std::chrono::duration<double>(std::chrono::high_resolution_clock::now() - t_fd_begin);

  if (!opt_fd.has_value()) {
    HEAT_WARN("Calculating gradient with finite differences failed.");
    return 1;
  }

  HEAT_INFO("Calculating gradient with finite differences took " << t_fd_dur.count() << "s");

  const auto& u_fd    = std::get<0>(*opt_fd);
  const auto& duda_fd = std::get<1>(*opt_fd);

  // - Algorithmic differentiation ---------------------------------------------
  const auto t_ad_begin = std::chrono::high_resolution_clock::now();
  const auto opt_ad     = duda_ad(u0, a, tend, dx, dt, mode);
  const auto t_ad_dur =
      std::chrono::duration<double>(std::chrono::high_resolution_clock::now() - t_ad_begin);

  if (!opt_ad.has_value()) {
    HEAT_WARN("Calculating gradient with algorithmic differentiation failed.");
    return 1;
  }

  HEAT_INFO("Calculating gradient with algorithmic differentiation took " << t_ad_dur.count()
                                                                          << "s");

  const auto& u_ad    = std::get<0>(*opt_ad);
  const auto& duda_ad = std::get<1>(*opt_ad);

  std::cout << "\n------------------------------------------------------------\n\n";

  // - Compate finite differences to algorithmic differentiation ---------------
  HEAT_INFO("||u_fd - u_ad||_2 = " << (u_fd - u_ad).lpNorm<2>());

  HEAT_INFO("||duda_fd - duda_ad||_1   = " << (duda_fd - duda_ad).lpNorm<1>());
  HEAT_INFO("||duda_fd - duda_ad||_2   = " << (duda_fd - duda_ad).lpNorm<2>());
  HEAT_INFO("||duda_fd - duda_ad||_inf = " << (duda_fd - duda_ad).lpNorm<Eigen::Infinity>());
  HEAT_INFO("avg_abs_error(duda_fd, duda_ad) = " << (duda_fd - duda_ad).lpNorm<Eigen::Infinity>() /
                                                        static_cast<Float>(duda_fd.rows()));

  std::cout << "\n------------------------------------------------------------\n\n";

  // - Save results to binary file ---------------------------------------------
  {
    constexpr auto OUTPUT_DIR = HEAT_STRINGIFY(HEAT_DATA_OUTPUT_DIR) "duda/";
    std::error_code ec;
    const auto _ = fs::create_directories(OUTPUT_DIR, ec);
    if (ec) {
      HEAT_WARN("Could not create directory " << OUTPUT_DIR << ": " << ec.message());
      return 1;
    }
  }

  {
    constexpr auto filename = HEAT_STRINGIFY(HEAT_DATA_OUTPUT_DIR) "duda/primal.npy";
    if (!Heat::save_to_npy(filename, u_ad)) {
      HEAT_WARN("Could not save `u_ad`");
      return 1;
    }
    HEAT_INFO("Saved `u_ad` to " << filename);
  }

  {
    constexpr auto filename = HEAT_STRINGIFY(HEAT_DATA_OUTPUT_DIR) "duda/ad.npy";
    if (!Heat::save_to_npy(filename, duda_ad)) {
      HEAT_WARN("Could not save `duda_ad`");
      return 1;
    }
    HEAT_INFO("Saved `duda_ad` to " << filename);
  }

  {
    constexpr auto filename = HEAT_STRINGIFY(HEAT_DATA_OUTPUT_DIR) "duda/fd.npy";
    if (!Heat::save_to_npy(filename, duda_fd)) {
      HEAT_WARN("Could not save `duda_fd`");
      return 1;
    }
    HEAT_INFO("Saved `duda_fd` to " << filename);
  }
}
