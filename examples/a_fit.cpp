#include <cmath>
#include <filesystem>
#include <numbers>
#include <numeric>
#include <random>

#include <AD/ad.hpp>

#define HEAT_DISABLE_IMPLICIT_EULER_WARNING
#include "Heat.hpp"

#define OUTPUT_DIR HEAT_STRINGIFY(HEAT_DATA_OUTPUT_DIR) "a_fit/"

// #define PRINT_DETAILS

// -------------------------------------------------------------------------------------------------
constexpr auto x_min = 0.0;
constexpr auto x_max = 2.0;

// -------------------------------------------------------------------------------------------------
template <typename Float>
struct Sensor {
  Float pos{};
  Float val{};
};

// -------------------------------------------------------------------------------------------------
template <typename ActiveFloat, typename PassiveFloat>
[[nodiscard]] auto
analytical_solution_step(PassiveFloat x, PassiveFloat t, ActiveFloat a) noexcept -> ActiveFloat {
// Defines a std::array `c` in the local scope containing the coefficients for the cosine transform
#include "./a_fit_cos_trans_coef.inc"
  static_assert(c.size() > 0);

  ActiveFloat res = c[0] / static_cast<PassiveFloat>(2);
  for (size_t n = 1; n < c.size(); ++n) {
    const auto factor =
        (static_cast<PassiveFloat>(n) * std::numbers::pi_v<PassiveFloat>) / (x_max - x_min);
    // NOLINTNEXTLINE
    res += c[n] * std::cos(factor * x) * std::exp(-a * std::pow(factor, 2) * t);
  }
  return res;
}

// -------------------------------------------------------------------------------------------------
template <typename ActiveFloat, typename PassiveFloat>
[[nodiscard]] auto
analytical_solution_smooth(PassiveFloat x, PassiveFloat t, ActiveFloat a) noexcept -> ActiveFloat {
  constexpr std::array z = {static_cast<PassiveFloat>(1.0), static_cast<PassiveFloat>(3.0)};

  ActiveFloat res = 0.0;
  for (auto zi : z) {
    res +=
        100 * std::cos((zi * std::numbers::pi_v<PassiveFloat>) / (x_max - x_min) * x) *
        std::exp(-a * std::pow((zi * std::numbers::pi_v<PassiveFloat>) / (x_max - x_min), 2) * t);
  }
  return res;
}

// -------------------------------------------------------------------------------------------------
template <typename ActiveFloat,
          typename PassiveFloat,
          typename SpaceGrid,
          typename Laplace,
          typename Boundary,
          typename Integrator>
[[nodiscard]] constexpr auto run_solver(PassiveFloat t_end,
                                        PassiveFloat dt,
                                        ActiveFloat a,
                                        const SpaceGrid& grid,
                                        Laplace laplace,
                                        Boundary boundary,
                                        Integrator integrator) noexcept -> SpaceGrid {
  // HEAT_INFO("Laplace    = " << Laplace::name());
  // HEAT_INFO("Boundary   = " << Boundary::name());
  // HEAT_INFO("Integrator = " << Integrator::name());

  auto rhs = Heat::Rhs::HeatEq{a, laplace};

  const auto solver =
      Heat::Solver<ActiveFloat, PassiveFloat, SpaceGrid, decltype(rhs), Boundary, Integrator>{
          std::move(rhs), std::move(integrator), std::move(boundary), std::move(grid)};

  const auto sol = solver.solve(t_end, dt);
  return sol;
}

// -------------------------------------------------------------------------------------------------
template <typename PassiveFloat,
          typename SpaceGrid,
          typename Laplace,
          typename Boundary,
          typename Integrator>
[[nodiscard]] constexpr auto jacobian_driver(const std::vector<Sensor<PassiveFloat>>& sensors,
                                             PassiveFloat t_end,
                                             PassiveFloat dt,
                                             PassiveFloat a,
                                             const SpaceGrid& grid,
                                             Laplace laplace,
                                             Boundary boundary,
                                             Integrator integrator) noexcept
    -> std::pair<Eigen::VectorX<PassiveFloat>, Eigen::VectorX<PassiveFloat>> {
  using ActiveFloat = ad::gt1s<PassiveFloat>::type;

  const ActiveFloat a_ad = a;
  ad::derivative(a_ad)   = 1.0;

  auto rhs = Heat::Rhs::HeatEq{a_ad, laplace};

  const auto solver =
      Heat::Solver<ActiveFloat, PassiveFloat, SpaceGrid, decltype(rhs), Boundary, Integrator>{
          std::move(rhs), std::move(integrator), std::move(boundary), std::move(grid)};

  const auto sol = solver.solve(t_end, dt);

  const auto n = static_cast<Eigen::Index>(sensors.size());

  Eigen::VectorX<ActiveFloat> error(n);
  std::transform(std::cbegin(sensors),
                 std::cend(sensors),
                 std::begin(error),
                 [&sol](Sensor<PassiveFloat> s) { return sol.eval_at(s.pos) - s.val; });

  Eigen::VectorX<PassiveFloat> diff(n);
  std::transform(std::cbegin(error), std::cend(error), std::begin(diff), [](const ActiveFloat& e) {
    return ad::value(e);
  });

  Eigen::VectorX<PassiveFloat> jac(n);
  std::transform(std::cbegin(error), std::cend(error), std::begin(jac), [](const ActiveFloat& e) {
    return ad::derivative(e);
  });

  return std::make_pair(diff, jac);
}

// -------------------------------------------------------------------------------------------------
template <typename PassiveFloat,
          typename SpaceGrid,
          typename Laplace,
          typename Boundary,
          typename Integrator>
[[nodiscard]] constexpr auto
levenberg_marquardt(const std::vector<Sensor<PassiveFloat>>& sensors,
                    PassiveFloat t_end,
                    PassiveFloat dt,
                    PassiveFloat a,
                    const SpaceGrid& grid,
                    Laplace laplace,
                    Boundary boundary,
                    Integrator integrator,
                    PassiveFloat alpha = 1.0,
                    PassiveFloat beta  = 0.1,
                    PassiveFloat tol   = 1e-3,
                    size_t max_iter    = 10'000UL) noexcept -> PassiveFloat {
#ifdef PRINT_DETAILS
  const auto t_begin = std::chrono::high_resolution_clock::now();
#endif  // PRINT_DETAILS

  auto [diff, jac] = jacobian_driver(sensors, t_end, dt, a, grid, laplace, boundary, integrator);
  auto whole_grad  = jac.dot(diff);
  auto residual    = std::abs(whole_grad);
  size_t iter      = 0UL;
  for (; residual > tol && iter < max_iter; ++iter) {
    PassiveFloat jtj = jac.dot(jac);

    const auto delta = beta;
    // const auto delta = beta * jac(0);
    // const auto delta = beta * jtj;

    const auto step = alpha * (jtj + delta) / whole_grad;

    auto [next_diff, next_jac] =
        jacobian_driver(sensors, t_end, dt, a - step, grid, laplace, boundary, integrator);
    auto next_whole_grad = next_jac.dot(next_diff);
    auto next_residual   = std::abs(next_whole_grad);

#ifdef PRINT_DETAILS
    HEAT_INFO("residual = " << residual);
    HEAT_INFO("next_residual = " << next_residual);
    HEAT_INFO("alpha = " << alpha);
    HEAT_INFO("beta = " << beta);
    HEAT_INFO("a = " << a);
    HEAT_INFO("next_a = " << a - step);
#endif  // PRINT_DETAILS

    // prev_diff_sqnorm = diff.squaredNorm();
    const auto diff_sqnorm      = diff.squaredNorm();
    const auto next_diff_sqnorm = next_diff.squaredNorm();

    const auto accept_measure =
        (diff_sqnorm - next_diff_sqnorm) / std::abs(step * (delta * step + whole_grad));

#ifdef PRINT_DETAILS
    HEAT_INFO("accept_measure = " << accept_measure);
#endif  // PRINT_DETAILS

#ifdef ADJUST_BETA
    // beta adjustment taken from: https://people.duke.edu/~hpgavin/ExperimentalSystems/lm.pdf
    constexpr auto max_beta = static_cast<PassiveFloat>(1e7);
    constexpr auto min_beta = static_cast<PassiveFloat>(1e-7);

    constexpr auto factor_accept_beta = static_cast<PassiveFloat>(11);
    constexpr auto factor_reject_beta = static_cast<PassiveFloat>(7);
#endif  // ADJUST_BETA

    constexpr auto max_alpha           = static_cast<PassiveFloat>(5);
    constexpr auto min_alpha           = static_cast<PassiveFloat>(1e-12);
    constexpr auto factor_accept_alpha = static_cast<PassiveFloat>(1);
    constexpr auto factor_reject_alpha = static_cast<PassiveFloat>(2);

    constexpr auto accept_tol = static_cast<PassiveFloat>(1e-1);
    if (accept_measure > accept_tol) {

#ifdef PRINT_DETAILS
      HEAT_INFO("ACCEPTED!");
#endif  // PRINT_DETAILS

      alpha = std::min(alpha * factor_accept_alpha, max_alpha);

#ifdef ADJUST_BETA
      beta = std::max(beta / factor_accept_beta, min_beta);
#endif  // ADJUST_BETA

      diff.swap(next_diff);
      jac.swap(next_jac);
      std::swap(whole_grad, next_whole_grad);
      residual = next_residual;

      a -= step;
    } else {

#ifdef PRINT_DETAILS
      HEAT_INFO("REJECTED!");
#endif  // PRINT_DETAILS

      alpha = std::max(alpha / factor_reject_alpha, min_alpha);

#ifdef ADJUST_BETA
      beta = std::min(beta * factor_reject_beta, max_beta);
#endif  // ADJUST_BETA
    }

#ifdef PRINT_DETAILS
    std::cout << "------------------------------------------------------------\n";
#endif  // PRINT_DETAILS
  }

  if (iter == max_iter && residual > tol) {
    HEAT_WARN("Optimization did not converge in " << iter << " iterations: residual is " << residual
                                                  << '.');
  }

#ifdef PRINT_DETAILS
  const auto t_dur =
      std::chrono::duration<double>{std::chrono::high_resolution_clock::now() - t_begin};
  HEAT_INFO("Optimizing took " << t_dur.count() << "s");
#endif  // PRINT_DETAILS
  return a;
}

// -------------------------------------------------------------------------------------------------
template <typename PassiveFloat,
          typename SpaceGrid,
          typename Laplace,
          typename Boundary,
          typename Integrator>
constexpr void fit_a(std::ofstream& out,
                     PassiveFloat a_target,
                     const std::vector<Sensor<PassiveFloat>>& sensors,
                     PassiveFloat t_end,
                     PassiveFloat dt,
                     PassiveFloat a,
                     const SpaceGrid& grid,
                     Laplace laplace,
                     Boundary boundary,
                     Integrator integrator,
                     PassiveFloat alpha = 1.0,
                     PassiveFloat beta  = 0.1,
                     PassiveFloat tol   = 1e-3,
                     size_t max_iter    = 10'000UL) noexcept {
  const auto grid_size = grid.size()[0];
  HEAT_INFO("Running " << Laplace::name() << ", " << Boundary::name() << ", " << Integrator::name()
                       << ", " << grid_size << "...");
  std::cout << std::flush;

  const auto t_begin_fit = std::chrono::high_resolution_clock::now();

  const auto a_fitted = levenberg_marquardt(
      sensors, t_end, dt, a, grid, laplace, boundary, integrator, alpha, beta, tol, max_iter);

  const auto t_dur_fit =
      std::chrono::duration<double>{std::chrono::high_resolution_clock::now() - t_begin_fit};

  // -------------------------
  const auto grid_func = Heat::Grid::Func::Uniform1D{grid_size, x_min, x_max};
  const auto u0_func   = [=](PassiveFloat x) -> PassiveFloat {
    return analytical_solution_smooth(x, 0.0, a_target);
  };
  auto fwd_grid = Heat::Grid::OneD<PassiveFloat, PassiveFloat>{grid_size, grid_func, u0_func};

  const auto t_begin_fwd_sol = std::chrono::high_resolution_clock::now();
  const auto fwd_t_end       = 2.0 * t_end;
  const auto fwd_sol = run_solver(fwd_t_end, dt, a_fitted, fwd_grid, laplace, boundary, integrator);
  const auto t_dur_fwd_sol =
      std::chrono::duration<double>{std::chrono::high_resolution_clock::now() - t_begin_fwd_sol};

  PassiveFloat mse = 0.0;
  for (auto i = fwd_grid.begin_interior(); i != fwd_grid.end_interior();
       i.advance_interior(fwd_grid.size())) {
    mse += std::pow(
        fwd_sol.value_at(i) - analytical_solution_smooth(fwd_sol.x_at(i), fwd_t_end, a_target), 2);
  }
  for (auto i = fwd_grid.begin_boundary(); i != fwd_grid.end_boundary();
       i.advance_boundary(fwd_grid.size())) {
    mse += std::pow(
        fwd_sol.value_at(i) - analytical_solution_smooth(fwd_sol.x_at(i), fwd_t_end, a_target), 2);
  }
  mse /= static_cast<PassiveFloat>(grid_size);
  // -------------------------

  out << Laplace::name() << ',';
  out << Boundary::name() << ',';
  out << Integrator::name() << ',';
  out << grid_size << ',';
  out << t_dur_fit.count() << ',';
  out << a_fitted << ',';
  out << std::abs(a_fitted - a_target) << ',';
  out << std::abs(a_fitted - a_target) / a_target << ',';
  out << t_dur_fwd_sol.count() << ',';
  out << mse << '\n';
}

// -------------------------------------------------------------------------------------------------
auto main() -> int {
  using PassiveFloat = double;
  using ActiveFloat  = ad::gt1s<PassiveFloat>::type;

  const auto a     = static_cast<PassiveFloat>(0.15);
  const auto t_end = static_cast<PassiveFloat>(1.0);

  const auto a_target = a + 0.05;
  std::vector<Sensor<PassiveFloat>> sensors{
      Sensor{.pos = std::lerp(x_min, x_max, 0.0)},
      Sensor{.pos = std::lerp(x_min, x_max, 1.0 / 3.0)},
      Sensor{.pos = std::lerp(x_min, x_max, 2.0 / 3.0)},
      Sensor{.pos = std::lerp(x_min, x_max, 1.0)},
  };

  unsigned seed = std::random_device{}();
  HEAT_INFO("random seed = " << seed);
  // const unsigned seed = std::random_device{}();
  std::mt19937 rng(seed);
  std::normal_distribution<PassiveFloat> dist{0.0, 2.5};
  for (auto& s : sensors) {
    s.val = analytical_solution_step(s.pos, t_end, a_target) + dist(rng);
  }

  for (const auto& s : sensors) {
    HEAT_INFO("{ .pos = " << s.pos << ", .val = " << s.val << " }");
  }

  constexpr auto alpha    = static_cast<PassiveFloat>(5e-5);
  constexpr auto beta     = static_cast<PassiveFloat>(1.0);
  constexpr auto tol      = static_cast<PassiveFloat>(1e-1);
  constexpr auto max_iter = 75UL;

  {
    std::error_code ec;
    [[maybe_unused]] const auto _ = std::filesystem::create_directories(OUTPUT_DIR, ec);
    if (ec) {
      HEAT_WARN("Could not create directory " OUTPUT_DIR ": " << ec.message());
      return 1;
    }
  }
  const auto* filename = OUTPUT_DIR "data.csv";
  std::ofstream out(filename);
  if (!out) {
    HEAT_WARN("Could not open file `" << filename << "` for writing: " << std::strerror(errno)
                                      << '.');
    return 1;
  }

  // Write csv header
  // clang-format off
  out << "laplace"         << ','  
      << "boundary"        << ','  
      << "integrator"      << ',' 
      << "space_grid_size" << ',' 
      << "inv_time_s"      << ','  
      << "a_fitted"        << ','  
      << "inv_abs_error"   << ',' 
      << "inv_rel_error"   << ','
      << "fwd_time_s"      << ','
      << "fwd_mse"         << '\n';
  // clang-format on

  for (size_t grid_size = 10; grid_size <= 200; grid_size += 10) {
    const auto grid_func = Heat::Grid::Func::Uniform1D{grid_size, x_min, x_max};
    const auto u0_func   = [=](PassiveFloat x) -> ActiveFloat {
      const auto x_mid = std::midpoint(x_min, x_max);
      return static_cast<ActiveFloat>(100) * static_cast<PassiveFloat>(x <= x_mid);
    };
    auto grid = Heat::Grid::OneD<ActiveFloat, PassiveFloat>{grid_size, grid_func, u0_func};

    const auto dt = static_cast<PassiveFloat>(0.1) * static_cast<PassiveFloat>(0.5) *
                    grid_func.dx() * grid_func.dx() / a;

    // NOLINTNEXTLINE
#define FIT_A(laplace, boundary, integrator)                                                       \
  fit_a(out,                                                                                       \
        a_target,                                                                                  \
        sensors,                                                                                   \
        t_end,                                                                                     \
        dt,                                                                                        \
        a,                                                                                         \
        grid,                                                                                      \
        (laplace),                                                                                 \
        (boundary),                                                                                \
        (integrator),                                                                              \
        alpha,                                                                                     \
        beta,                                                                                      \
        tol,                                                                                       \
        max_iter)

    constexpr auto rk45_eps      = 1e-6;
    constexpr auto rk45_max_iter = 10UL;
    constexpr auto rk45_small_dt = 1e-8;

    FIT_A(Heat::Laplace::CentralFD{},
          (Heat::Boundary::Neumann<PassiveFloat, 1>{0.0, 0.0}),
          Heat::TimeIntegration::ExplicitEuler{});
    FIT_A(Heat::Laplace::CentralFD{},
          (Heat::Boundary::Neumann<PassiveFloat, 1>{0.0, 0.0}),
          Heat::TimeIntegration::ImplicitEuler{});
    FIT_A(Heat::Laplace::CentralFD{},
          (Heat::Boundary::Neumann<PassiveFloat, 1>{0.0, 0.0}),
          Heat::TimeIntegration::RK4{});
    FIT_A(Heat::Laplace::CentralFD{},
          (Heat::Boundary::Neumann<PassiveFloat, 1>{0.0, 0.0}),
          (Heat::TimeIntegration::RK45{rk45_eps, rk45_max_iter, rk45_small_dt}));

    FIT_A(Heat::Laplace::CentralFD{},
          (Heat::Boundary::Neumann<PassiveFloat, 2>{0.0, 0.0}),
          Heat::TimeIntegration::ExplicitEuler{});
    FIT_A(Heat::Laplace::CentralFD{},
          (Heat::Boundary::Neumann<PassiveFloat, 2>{0.0, 0.0}),
          Heat::TimeIntegration::ImplicitEuler{});
    FIT_A(Heat::Laplace::CentralFD{},
          (Heat::Boundary::Neumann<PassiveFloat, 2>{0.0, 0.0}),
          Heat::TimeIntegration::RK4{});
    FIT_A(Heat::Laplace::CentralFD{},
          (Heat::Boundary::Neumann<PassiveFloat, 2>{0.0, 0.0}),
          (Heat::TimeIntegration::RK45{rk45_eps, rk45_max_iter, rk45_small_dt}));

    FIT_A(Heat::Laplace::CentralFD4{},
          (Heat::Boundary::Neumann<PassiveFloat, 1>{0.0, 0.0}),
          Heat::TimeIntegration::ExplicitEuler{});
    FIT_A(Heat::Laplace::CentralFD4{},
          (Heat::Boundary::Neumann<PassiveFloat, 1>{0.0, 0.0}),
          Heat::TimeIntegration::ImplicitEuler{});
    FIT_A(Heat::Laplace::CentralFD4{},
          (Heat::Boundary::Neumann<PassiveFloat, 1>{0.0, 0.0}),
          Heat::TimeIntegration::RK4{});
    FIT_A(Heat::Laplace::CentralFD4{},
          (Heat::Boundary::Neumann<PassiveFloat, 1>{0.0, 0.0}),
          (Heat::TimeIntegration::RK45{rk45_eps, rk45_max_iter, rk45_small_dt}));

    FIT_A(Heat::Laplace::CentralFD4{},
          (Heat::Boundary::Neumann<PassiveFloat, 2>{0.0, 0.0}),
          Heat::TimeIntegration::ExplicitEuler{});
    FIT_A(Heat::Laplace::CentralFD4{},
          (Heat::Boundary::Neumann<PassiveFloat, 2>{0.0, 0.0}),
          Heat::TimeIntegration::ImplicitEuler{});
    FIT_A(Heat::Laplace::CentralFD4{},
          (Heat::Boundary::Neumann<PassiveFloat, 2>{0.0, 0.0}),
          Heat::TimeIntegration::RK4{});
    FIT_A(Heat::Laplace::CentralFD4{},
          (Heat::Boundary::Neumann<PassiveFloat, 2>{0.0, 0.0}),
          (Heat::TimeIntegration::RK45{rk45_eps, rk45_max_iter, rk45_small_dt}));
  }

  HEAT_INFO("Wrote data into `" << filename << "`.");
}
