#include <chrono>
#include <filesystem>
#include <iostream>
#include <numbers>
#include <random>
namespace fs = std::filesystem;

#include <AD/ad.hpp>

#include <PerlinNoise/PerlinNoise.hpp>

#include "Heat.hpp"

// #define PRINT_DETAILS
#define OUTPUT_DIR HEAT_STRINGIFY(HEAT_DATA_OUTPUT_DIR) "heat_fit/"

// - Parameters ------------------------------------------------------------------------------------
constexpr double a     = 0.15;
constexpr double tend  = 1.0;
constexpr double x_min = 0.0;
constexpr double x_max = 1.0;
constexpr auto calc_dt = []<typename Float>(Float dx) constexpr noexcept -> Float {
  return static_cast<Float>(0.1) * static_cast<Float>(0.5) * dx * dx / a;
};

// -------------------------------------------------------------------------------------------------
template <typename Float>
[[nodiscard]] constexpr auto analytical_solution(Float x, Float t) noexcept -> Float {
  constexpr std::array z = {static_cast<Float>(1.0), static_cast<Float>(3.0)};

  auto res = static_cast<Float>(0);
  for (auto zi : z) {
    res += std::cos((zi * std::numbers::pi_v<Float>) / (x_max - x_min) * x) *
           std::exp(-a * std::pow((zi * std::numbers::pi_v<Float>) / (x_max - x_min), 2) * t);
  }
  return res;
}

// -------------------------------------------------------------------------------------------------
template <typename ActiveFloat,
          typename PassiveFloat,
          typename LaplaceOp,
          typename Integrator,
          typename Boundary>
[[nodiscard]] constexpr auto run_solver(size_t n,
                                        const Eigen::VectorX<ActiveFloat>& u0,
                                        LaplaceOp laplace,
                                        Integrator integrator,
                                        Boundary boundary) noexcept -> Eigen::VectorX<ActiveFloat> {
  assert(u0.rows() == static_cast<Eigen::Index>(n));

  const auto grid_func = Heat::Grid::Func::Uniform1D(n, x_min, x_max);

  auto u0_func = [&](PassiveFloat x) -> ActiveFloat {
    const auto idx = static_cast<Eigen::Index>(std::round((x - x_min) / grid_func.dx()));
    return u0(idx);
  };

  auto grid = Heat::Grid::OneD<ActiveFloat, PassiveFloat>(n, grid_func, u0_func);

  static_assert(Heat::LaplaceOp_c<LaplaceOp, ActiveFloat, PassiveFloat, decltype(grid)>,
                "Type `LaplaceOp` does not satisfy the required conditions.");
  auto rhs = Heat::Rhs::HeatEq{a, std::move(laplace)};

  static_assert(Heat::Boundary_c<Boundary, ActiveFloat, decltype(grid)>,
                "Type `Boundary` does not satisfy the required conditions.");

  static_assert(Heat::TimeIntegrator_c<Integrator,
                                       ActiveFloat,
                                       PassiveFloat,
                                       decltype(grid),
                                       decltype(rhs),
                                       Boundary>,
                "Type `Integrator` does not satisfy the required conditions.");

  const auto solver = Heat::Solver<ActiveFloat,
                                   PassiveFloat,
                                   decltype(grid),
                                   decltype(rhs),
                                   decltype(boundary),
                                   decltype(integrator)>{
      std::move(rhs), std::move(integrator), std::move(boundary), std::move(grid)};

  const auto dt = calc_dt(grid_func.dx());

  // const auto t_begin = std::chrono::high_resolution_clock::now();
  const auto u = solver.solve(tend, dt);
  // const auto t_dur =
  //     std::chrono::duration<double>{std::chrono::high_resolution_clock::now() - t_begin};
  // HEAT_INFO("Solver took " << t_dur.count() << "s");

  return u.values();
}

// -------------------------------------------------------------------------------------------------
template <typename ActiveFloat,
          typename PassiveFloat,
          typename LaplaceOp,
          typename Integrator,
          typename Boundary>
[[nodiscard]] constexpr auto run_and_eval_solver(size_t n,
                                                 const Eigen::VectorX<ActiveFloat>& u0,
                                                 LaplaceOp laplace,
                                                 Integrator integrator,
                                                 Boundary boundary) noexcept
    -> std::pair<Eigen::VectorX<ActiveFloat>, Eigen::Vector<ActiveFloat, 5>> {
  assert(u0.rows() == static_cast<Eigen::Index>(n));
  const auto grid_func = Heat::Grid::Func::Uniform1D(n, x_min, x_max);

  auto u0_func = [&](PassiveFloat x) -> ActiveFloat {
    const auto idx = static_cast<Eigen::Index>(std::round((x - x_min) / grid_func.dx()));
    return u0(idx);
  };

  auto grid = Heat::Grid::OneD<ActiveFloat, PassiveFloat>(n, grid_func, u0_func);
  auto rhs  = Heat::Rhs::HeatEq{a, std::move(laplace)};
  const auto solver =
      Heat::Solver<ActiveFloat, PassiveFloat, decltype(grid), decltype(rhs), Boundary, Integrator>{
          std::move(rhs), std::move(integrator), std::move(boundary), std::move(grid)};

  const auto dt = calc_dt(grid_func.dx());

  auto F = [t = static_cast<PassiveFloat>(0)](const Heat::Grid::OneD<ActiveFloat, PassiveFloat>& u,
                                              PassiveFloat dt,
                                              Eigen::Vector<ActiveFloat, 5> prev_value) mutable {
    t += dt;
    constexpr std::array<PassiveFloat, 5> sensor_pos = {
        x_min,
        0.25 * (x_max - x_min) + x_min,
        0.5 * (x_max - x_min) + x_min,
        0.75 * (x_max - x_min) + x_min,
        x_max,
    };

    assert(sensor_pos.size() == static_cast<size_t>(prev_value.rows()));
    for (int i = 0; i < prev_value.rows(); ++i) {
      // NOLINTBEGIN(cppcoreguidelines-pro-bounds-constant-array-index)
      const auto actual    = analytical_solution(sensor_pos[static_cast<size_t>(i)], t);
      const auto numerical = u.eval_at(sensor_pos[static_cast<size_t>(i)]);
      // NOLINTEND(cppcoreguidelines-pro-bounds-constant-array-index)
      prev_value(i) += std::pow(actual - numerical, 2) * dt;
    }

    return prev_value;
  };
  Eigen::Vector<ActiveFloat, 5> functional_value = Eigen::Vector<ActiveFloat, 5>::Zero();

  const auto u_estimate = solver.solve_and_evaluate(tend, dt, F, functional_value).values();

  return std::make_pair(u_estimate, functional_value);
}

// -------------------------------------------------------------------------------------------------
template <typename PassiveFloat, typename LaplaceOp, typename Integrator, typename Boundary>
[[nodiscard]] auto jacobian_driver(const Eigen::VectorX<PassiveFloat>& u0,
                                   const Eigen::VectorX<PassiveFloat>& u_target,
                                   LaplaceOp laplace,
                                   Integrator integrator,
                                   Boundary boundary) noexcept
    -> std::pair<Eigen::VectorX<PassiveFloat>, Eigen::MatrixX<PassiveFloat>> {
  assert(u0.rows() == u_target.rows() && u0.rows() > 0);
#ifdef PRINT_DETAILS
  const auto t_begin = std::chrono::high_resolution_clock::now();
#endif  // PRINT_DETAILS
  const auto n = static_cast<size_t>(u0.rows());

  using AD_mode     = ad::gt1s<PassiveFloat>;
  using ActiveFloat = AD_mode::type;

  Eigen::VectorX<ActiveFloat> u0_ad = u0;

  Eigen::VectorX<PassiveFloat> diff(n);
  Eigen::MatrixX<PassiveFloat> jac(n, n);

#pragma omp parallel for firstprivate(u0_ad, laplace, integrator, boundary)
  for (Eigen::Index col = 0; col < static_cast<Eigen::Index>(n); ++col) {
    ad::derivative(u0_ad(col)) = 1.0;
    Eigen::VectorX<ActiveFloat> diff_ad =
        run_solver<ActiveFloat, PassiveFloat>(n, u0_ad, laplace, integrator, boundary) - u_target;
    for (Eigen::Index row = 0; row < static_cast<Eigen::Index>(n); ++row) {
      jac(row, col) = ad::derivative(diff_ad(row));
    }
    ad::derivative(u0_ad(col)) = 0.0;

    if (col == 0) {
      std::transform(std::cbegin(diff_ad), std::cend(diff_ad), std::begin(diff), [](const auto& x) {
        return ad::value(x);
      });
    }
  }

#ifdef PRINT_DETAILS
  const auto t_dur =
      std::chrono::duration<double>{std::chrono::high_resolution_clock::now() - t_begin};
  HEAT_INFO("Jacobian calculation took " << t_dur.count() << "s");
#endif  // PRINT_DETAILS
  return std::make_pair(diff, jac);
}

// -------------------------------------------------------------------------------------------------
template <typename Float, typename LaplaceOp, typename Integrator, typename Boundary>
[[nodiscard]] constexpr auto
levenberg_marquardt(Eigen::VectorX<Float> u0,
                    const Eigen::VectorX<Float>& u_target,
                    LaplaceOp laplace,
                    Integrator integrator,
                    Boundary boundary,
                    Float alpha     = 1.0,
                    Float beta      = 0.1,
                    Float tol       = 1e-3,
                    size_t max_iter = 10'000UL) noexcept -> Eigen::VectorX<Float> {
  assert(u0.rows() == u_target.rows() && u0.rows() > 0);
  [[maybe_unused]] const auto n = u0.rows();

  const auto t_begin = std::chrono::high_resolution_clock::now();

  auto [diff, jac] = jacobian_driver(u0, u_target, laplace, integrator, boundary);
  auto whole_grad  = (jac.transpose() * diff).eval();
  auto residual    = whole_grad.norm();
  size_t iter      = 0UL;
  for (; residual > tol && iter < max_iter; ++iter) {
    const Eigen::MatrixX<Float> jtj = jac.transpose() * jac;

    // const auto delta = beta * Eigen::MatrixX<Float>::Identity(n, n);
    const Eigen::MatrixX<Float> delta = beta * jac.diagonal().asDiagonal();
    // const Eigen::MatrixX<Float> delta = beta * jtj.diagonal().asDiagonal();

    const Eigen::VectorX<Float> step = alpha * (jtj + delta).ldlt().solve(whole_grad);

    auto [next_diff, next_jac] =
        jacobian_driver((u0 - step).eval(), u_target, laplace, integrator, boundary);
    auto next_whole_grad = (next_jac.transpose() * next_diff).eval();
    auto next_residual   = next_whole_grad.norm();

#ifdef PRINT_DETAILS
    // const Eigen::MatrixX<Float> jtjd = jac.transpose() * jac + delta;
    // HEAT_INFO("condition = " << jtjd.norm() * jtjd.inverse().norm());
    HEAT_INFO("residual = " << residual);
    HEAT_INFO("next_residual = " << next_residual);
    HEAT_INFO("beta = " << beta);
#endif  // PRINT_DETAILS

    // prev_diff_sqnorm = diff.squaredNorm();
    const auto diff_sqnorm      = diff.squaredNorm();
    const auto next_diff_sqnorm = next_diff.squaredNorm();

    const auto accept_measure =
        (diff_sqnorm - next_diff_sqnorm) / std::abs(step.dot(delta * step + whole_grad));

#ifdef PRINT_DETAILS
    HEAT_INFO("accept_measure = " << accept_measure);
    std::cout << "------------------------------------------------------------\n";
#endif  // PRINT_DETAILS

    // beta adjustment taken from: https://people.duke.edu/~hpgavin/ExperimentalSystems/lm.pdf
    constexpr auto max_beta = static_cast<Float>(1e7);
    constexpr auto min_beta = static_cast<Float>(1e-7);

    constexpr auto factor_accept = static_cast<Float>(11);
    constexpr auto factor_reject = static_cast<Float>(7);
    constexpr auto accept_tol    = static_cast<Float>(1e-1);

    if (accept_measure > accept_tol) {
      beta = std::max(beta / factor_accept, min_beta);

      diff.swap(next_diff);
      jac.swap(next_jac);
      whole_grad.swap(next_whole_grad);
      residual = next_residual;

      u0 -= step;
    } else {
      beta = std::min(beta * factor_reject, max_beta);
    }
  }

  if (iter == max_iter) {
    HEAT_WARN("Optimization did not converge in " << iter << " iterations: residual is " << residual
                                                  << '.');
  }

  const auto t_dur =
      std::chrono::duration<double>{std::chrono::high_resolution_clock::now() - t_begin};
  HEAT_INFO("Optimizing took " << t_dur.count() << "s");
  return u0;
}

// -------------------------------------------------------------------------------------------------
template <typename Float>
[[nodiscard]] auto generate_target(size_t n, Float std_dev = 0.1, uint32_t seed = 42) noexcept
    -> std::tuple<Eigen::VectorX<Float>, Eigen::VectorX<Float>, Eigen::VectorX<Float>> {
  Eigen::VectorX<Float> x(n);
  for (int i = 0; i < static_cast<int>(n); ++i) {
    x(i) = static_cast<Float>(i) / static_cast<Float>(n - 1);
  }

  Eigen::VectorX<Float> u0_target(n);
  Eigen::VectorX<Float> u_target(n);
  for (int i = 0; i < static_cast<int>(n); ++i) {
    u0_target(i) = analytical_solution(x(i), static_cast<Float>(0));
    u_target(i)  = analytical_solution(x(i), tend);
  }

  if (std_dev > static_cast<Float>(0)) {
    std::mt19937 gen{seed};
    std::normal_distribution<Float> dist(static_cast<Float>(0), std_dev);
    for (int i = 0; i < static_cast<int>(n); ++i) {
      u_target(i) += dist(gen);
    }
  }

  return std::make_tuple(x, u0_target, u_target);
}

// -------------------------------------------------------------------------------------------------
template <typename Float, typename LaplaceOp, typename Integrator, typename Boundary>
[[nodiscard]] constexpr auto run_optimizer(Eigen::VectorX<Float> u0,
                                           const Eigen::VectorX<Float>& u_target,
                                           LaplaceOp laplace,
                                           Integrator integrator,
                                           Boundary boundary,
                                           const std::string& shorthand_name,
                                           Float alpha     = 1.0,
                                           Float beta      = 0.1,
                                           Float tol       = 1e-3,
                                           size_t max_iter = 10'000UL) noexcept -> bool {
  std::cout << "----------------------------------------\n";
  HEAT_INFO("laplace    = " << decltype(laplace)::name());
  HEAT_INFO("integrator = " << decltype(integrator)::name());
  HEAT_INFO("boundary   = " << decltype(boundary)::name());

  const auto u0_estimate = levenberg_marquardt<Float, LaplaceOp, Integrator, Boundary>(
      u0, u_target, laplace, integrator, boundary, alpha, beta, tol, max_iter);

  {
    const auto output_file = OUTPUT_DIR "u0_" + shorthand_name + ".npy";
    if (!Heat::save_to_npy(output_file, u0_estimate)) {
      HEAT_WARN("Could not save u0_estimate to " << output_file);
      return false;
    }
    HEAT_INFO("Wrote u0_estimate to " << output_file);
  }

  const auto [u_estimate, functional_value] = run_and_eval_solver<Float, Float>(
      static_cast<size_t>(u0_estimate.rows()), u0_estimate, laplace, integrator, boundary);

  {
    const auto output_file = OUTPUT_DIR "u_" + shorthand_name + ".npy";
    if (!Heat::save_to_npy(output_file, u_estimate)) {
      HEAT_WARN("Could not save u_estimate to " << output_file);
      return false;
    }
    HEAT_INFO("Wrote u_estimate to " << output_file);
  }

  HEAT_INFO("functional_value = " << functional_value.transpose());

  return true;
}

// -------------------------------------------------------------------------------------------------
auto main() -> int {
  using Float = double;

  constexpr size_t n                  = 100;
  const auto [x, u0_target, u_target] = generate_target<Float>(n, 5e-3, std::random_device{}());

  {
    std::error_code ec;
    const auto _ = fs::create_directories(OUTPUT_DIR, ec);
    if (ec) {
      HEAT_WARN("Could not create directory " OUTPUT_DIR ": " << ec.message());
      return 1;
    }
  }

  {
    constexpr auto output_file = OUTPUT_DIR "x.npy";
    if (!Heat::save_to_npy(output_file, x)) {
      HEAT_WARN("Could not save x to " << output_file);
      return 1;
    }
    HEAT_INFO("Wrote x to " << output_file);
  }

  {
    constexpr auto output_file = OUTPUT_DIR "u0_target.npy";
    if (!Heat::save_to_npy(output_file, u0_target)) {
      HEAT_WARN("Could not save u0_target to " << output_file);
      return 1;
    }
    HEAT_INFO("Wrote u0_target to " << output_file);
  }

  {
    constexpr auto output_file = OUTPUT_DIR "u_target.npy";
    if (!Heat::save_to_npy(output_file, u_target)) {
      HEAT_WARN("Could not save u_target to " << output_file);
      return 1;
    }
    HEAT_INFO("Wrote u_target to " << output_file);
  }

  // - Random u0 -------------
  // const Eigen::VectorX<Float> u0 =
  //     50.0 * (Eigen::VectorX<Float>::Random(n) + Eigen::VectorX<Float>::Ones(n));

  // - Step function u0 ------
  // Eigen::VectorX<Float> u0(n);
  // std::transform(std::cbegin(x), std::cend(x), std::begin(u0), [](const auto& xi) {
  //   return static_cast<Float>(100) * (xi <= static_cast<Float>(0.5));
  // });

  // - Constant u0 -----------
  // const auto mean_u_target =
  //     std::reduce(std::cbegin(u_target), std::cend(u_target)) /
  //     static_cast<Float>(u_target.rows());
  // const Eigen::VectorX<Float> u0 = mean_u_target * Eigen::VectorX<Float>::Ones(n);

  // - Perturbed u0_target (gaussian)
  // std::mt19937 gen{std::random_device{}()};
  // std::normal_distribution<Float> dist(static_cast<Float>(0), 1);
  // Eigen::VectorX<Float> u0(n);
  // std::transform(std::cbegin(u0_target),
  //                std::cend(u0_target),
  //                std::begin(u0),
  //                [&](const auto& u0_target_i) { return u0_target_i + dist(gen); });

  // - Perturbed u0_target (perlin)
  siv::PerlinNoise noise{std::random_device{}()};
  Eigen::VectorX<Float> u0(n);
  constexpr auto freq  = static_cast<Float>(1);
  constexpr auto scale = static_cast<Float>(1);
  for (Eigen::Index i = 0; i < static_cast<Eigen::Index>(n); ++i) {
    u0(i) = u0_target(i) + noise.noise1D(freq * x(i)) / scale;
  }

  // - u0 = u_target  --------
  // Eigen::VectorX<Float> u0 = u_target;

  {
    constexpr auto output_file = OUTPUT_DIR "u0_initial.npy";
    if (!Heat::save_to_npy(output_file, u0)) {
      HEAT_WARN("Could not save u0 to " << output_file);
      return 1;
    }
    HEAT_INFO("Wrote u0 to " << output_file);
  }

  {
    HEAT_INFO("No optimizing:");
    const auto laplace                        = Heat::Laplace::CentralFD{};
    const auto integrator                     = Heat::TimeIntegration::RK4{};
    const auto boundary                       = Heat::Boundary::Neumann<Float, 2>{0.0, 0.0};
    const auto [u_estimate, functional_value] = run_and_eval_solver<Float, Float>(
        static_cast<size_t>(u0.rows()), u0, laplace, integrator, boundary);

    {
      const auto* const output_file = OUTPUT_DIR "u_no_fit.npy";
      if (!Heat::save_to_npy(output_file, u_estimate)) {
        HEAT_WARN("Could not save u_estimate to " << output_file);
        return 1;
      }
      HEAT_INFO("Wrote u_estimate to " << output_file);
    }

    HEAT_INFO("functional_value = " << functional_value.transpose());
  }

  constexpr auto alpha    = static_cast<Float>(1.0);
  constexpr auto beta     = static_cast<Float>(1.0);
  constexpr auto tol      = static_cast<Float>(5e-3);  //  static_cast<Float>(1e-5);
  constexpr auto max_iter = 100UL;

  if (!run_optimizer(u0,
                     u_target,
                     Heat::Laplace::CentralFD{},
                     Heat::TimeIntegration::ExplicitEuler{},
                     Heat::Boundary::Neumann<Float, 1>{0.0, 0.0},
                     "cfd2_ee_n1",
                     alpha,
                     beta,
                     tol,
                     max_iter)) {
    return 1;
  }

  if (!run_optimizer(u0,
                     u_target,
                     Heat::Laplace::CentralFD{},
                     Heat::TimeIntegration::ImplicitEuler{},
                     Heat::Boundary::Neumann<Float, 1>{0.0, 0.0},
                     "cfd2_ie_n1",
                     alpha,
                     beta,
                     tol,
                     max_iter)) {
    return 1;
  }

  if (!run_optimizer(u0,
                     u_target,
                     Heat::Laplace::CentralFD{},
                     Heat::TimeIntegration::RK4{},
                     Heat::Boundary::Neumann<Float, 1>{0.0, 0.0},
                     "cfd2_rk4_n1",
                     alpha,
                     beta,
                     tol,
                     max_iter)) {
    return 1;
  }

  if (!run_optimizer(u0,
                     u_target,
                     Heat::Laplace::CentralFD{},
                     Heat::TimeIntegration::RK45{},
                     Heat::Boundary::Neumann<Float, 1>{0.0, 0.0},
                     "cfd2_rk45_n1",
                     alpha,
                     beta,
                     tol,
                     max_iter)) {
    return 1;
  }

  std::cout << "============================================================\n";

  if (!run_optimizer(u0,
                     u_target,
                     Heat::Laplace::CentralFD{},
                     Heat::TimeIntegration::ExplicitEuler{},
                     Heat::Boundary::Neumann<Float, 2>{0.0, 0.0},
                     "cfd2_ee_n2",
                     alpha,
                     beta,
                     tol,
                     max_iter)) {
    return 1;
  }

  if (!run_optimizer(u0,
                     u_target,
                     Heat::Laplace::CentralFD{},
                     Heat::TimeIntegration::ImplicitEuler{},
                     Heat::Boundary::Neumann<Float, 2>{0.0, 0.0},
                     "cfd2_ie_n2",
                     alpha,
                     beta,
                     tol,
                     max_iter)) {
    return 1;
  }

  if (!run_optimizer(u0,
                     u_target,
                     Heat::Laplace::CentralFD{},
                     Heat::TimeIntegration::RK4{},
                     Heat::Boundary::Neumann<Float, 2>{0.0, 0.0},
                     "cfd2_rk4_n2",
                     alpha,
                     beta,
                     tol,
                     max_iter)) {
    return 1;
  }

  if (!run_optimizer(u0,
                     u_target,
                     Heat::Laplace::CentralFD{},
                     Heat::TimeIntegration::RK45{},
                     Heat::Boundary::Neumann<Float, 2>{0.0, 0.0},
                     "cfd2_rk45_n2",
                     alpha,
                     beta,
                     tol,
                     max_iter)) {
    return 1;
  }

  std::cout << "============================================================\n";

  if (!run_optimizer(u0,
                     u_target,
                     Heat::Laplace::CentralFD4{},
                     Heat::TimeIntegration::ExplicitEuler{},
                     Heat::Boundary::Neumann<Float, 2>{0.0, 0.0},
                     "cfd4_ee_n2",
                     alpha,
                     beta,
                     tol,
                     max_iter)) {
    return 1;
  }

  if (!run_optimizer(u0,
                     u_target,
                     Heat::Laplace::CentralFD4{},
                     Heat::TimeIntegration::ImplicitEuler{},
                     Heat::Boundary::Neumann<Float, 2>{0.0, 0.0},
                     "cfd4_ie_n2",
                     alpha,
                     beta,
                     tol,
                     max_iter)) {
    return 1;
  }

  if (!run_optimizer(u0,
                     u_target,
                     Heat::Laplace::CentralFD4{},
                     Heat::TimeIntegration::RK4{},
                     Heat::Boundary::Neumann<Float, 2>{0.0, 0.0},
                     "cfd4_rk4_n2",
                     alpha,
                     beta,
                     tol,
                     max_iter)) {
    return 1;
  }

  if (!run_optimizer(u0,
                     u_target,
                     Heat::Laplace::CentralFD4{},
                     Heat::TimeIntegration::RK45{},
                     Heat::Boundary::Neumann<Float, 2>{0.0, 0.0},
                     "cfd4_rk45_n2",
                     alpha,
                     beta,
                     tol,
                     max_iter)) {
    return 1;
  }
}
