#include <iostream>
#include <numbers>

#include <AD/ad.hpp>

#include "Heat.hpp"

template <typename ActiveFloat,
          typename PassiveFloat,
          typename LaplaceOp,
          typename Boundary,
          typename Integrator>
[[nodiscard]] constexpr auto run_solver(const ActiveFloat& p,
                                        LaplaceOp laplace,
                                        Boundary boundary,
                                        Integrator integrator) -> Eigen::VectorX<ActiveFloat> {
  // - Create space grid ---------------
  constexpr PassiveFloat x_min = 0.0;
  constexpr PassiveFloat x_max = 1.0;
  constexpr size_t n           = 50;
  constexpr auto dx            = (x_max - x_min) / static_cast<PassiveFloat>(n - 1UL);

  constexpr auto grid_func = [&](Heat::Grid::Idx<1> i) {
    const auto x = x_min + dx * static_cast<PassiveFloat>(i[0]);
    return x;
  };

  const auto u0_func = [&](PassiveFloat x) -> ActiveFloat {
    return p * static_cast<PassiveFloat>(0.5) *
           (std::cos(std::numbers::pi_v<PassiveFloat> * x) + 1.0);
  };

  auto x_grid = Heat::Grid::OneD<ActiveFloat, PassiveFloat>(n, grid_func, u0_func);

  // - Create right-hand side ----------
  constexpr auto a   = static_cast<PassiveFloat>(0.15);
  constexpr auto rhs = Heat::Rhs::HeatEq{a, std::move(laplace)};

  const Heat::Solver<ActiveFloat,
                     PassiveFloat,
                     decltype(x_grid),
                     decltype(rhs),
                     decltype(boundary),
                     decltype(integrator)>
      solver(rhs, std::move(integrator), std::move(boundary), x_grid);

  constexpr auto tend = static_cast<PassiveFloat>(1.0);
  const auto dt       = 0.5 * dx * dx * dx * dx / a;

  const auto u = solver.solve(tend, dt);

  return u.values();
}

template <typename PassiveFloat, typename LaplaceOp, typename Boundary, typename Integrator>
[[nodiscard]] constexpr auto fd_driver(PassiveFloat p,
                                       PassiveFloat eps,
                                       LaplaceOp laplace,
                                       Boundary boundary,
                                       Integrator integrator) -> Eigen::VectorX<PassiveFloat> {
  const auto u_minus =
      run_solver<PassiveFloat, PassiveFloat>(p - eps, laplace, boundary, integrator);
  const auto u_plus =
      run_solver<PassiveFloat, PassiveFloat>(p + eps, laplace, boundary, integrator);

  return (u_plus - u_minus) / (static_cast<PassiveFloat>(2) * eps);
}

template <typename PassiveFloat, typename LaplaceOp, typename Boundary, typename Integrator>
[[nodiscard]] constexpr auto ad_driver(PassiveFloat p,
                                       LaplaceOp laplace,
                                       Boundary boundary,
                                       Integrator integrator) -> Eigen::VectorX<PassiveFloat> {
  using ActiveFloat = ad::gt1s<PassiveFloat>::type;

  const ActiveFloat p_act = p;
  ad::derivative(p_act)   = 1.0;

  const auto u = run_solver<ActiveFloat, PassiveFloat>(p_act, laplace, boundary, integrator);
  Eigen::VectorX<PassiveFloat> grad(u.rows());

  std::transform(std::cbegin(u), std::cend(u), std::begin(grad), [](const auto& ui) {
    return ad::derivative(ui);
  });
  return grad;
}

template <typename PassiveFloat, typename LaplaceOp, typename Boundary, typename Integrator>
constexpr void compare_derivatives(
    PassiveFloat p, PassiveFloat eps, LaplaceOp laplace, Boundary boundary, Integrator integrator) {
  HEAT_INFO("laplace    = " << decltype(laplace)::name());
  HEAT_INFO("integrator = " << decltype(integrator)::name());
  HEAT_INFO("boundary   = " << decltype(boundary)::name());

  const auto grad_fd = fd_driver(p, eps, laplace, boundary, integrator);
  const auto grad_ad = ad_driver(p, laplace, boundary, integrator);

  HEAT_INFO("||grad_fd - grad_ad|| = " << (grad_fd - grad_ad).norm());
  std::cout << "----------------------------------------\n";
}

auto main() -> int {
  using PassiveFloat = double;
  constexpr auto p   = static_cast<PassiveFloat>(10);
  const auto eps     = std::sqrt(std::numeric_limits<PassiveFloat>::epsilon());

  compare_derivatives(p,
                      eps,
                      Heat::Laplace::CentralFD{},
                      Heat::Boundary::Dirichlet{p, 0.0},
                      Heat::TimeIntegration::ExplicitEuler{});

  compare_derivatives(p,
                      eps,
                      Heat::Laplace::CentralFD{},
                      Heat::Boundary::Dirichlet{p, 0.0},
                      Heat::TimeIntegration::ImplicitEuler{});

  compare_derivatives(p,
                      eps,
                      Heat::Laplace::CentralFD{},
                      Heat::Boundary::Dirichlet{p, 0.0},
                      Heat::TimeIntegration::RK4{});

  compare_derivatives(p,
                      eps,
                      Heat::Laplace::CentralFD{},
                      Heat::Boundary::Dirichlet{p, 0.0},
                      Heat::TimeIntegration::RK45{});

  std::cout << "============================================================\n";

  compare_derivatives(p,
                      eps,
                      Heat::Laplace::CentralFD{},
                      Heat::Boundary::Neumann<PassiveFloat, 1>{0.0, 0.0},
                      Heat::TimeIntegration::ExplicitEuler{});

  compare_derivatives(p,
                      eps,
                      Heat::Laplace::CentralFD{},
                      Heat::Boundary::Neumann<PassiveFloat, 1>{0.0, 0.0},
                      Heat::TimeIntegration::ImplicitEuler{});

  compare_derivatives(p,
                      eps,
                      Heat::Laplace::CentralFD{},
                      Heat::Boundary::Neumann<PassiveFloat, 1>{0.0, 0.0},
                      Heat::TimeIntegration::RK4{});

  compare_derivatives(p,
                      eps,
                      Heat::Laplace::CentralFD{},
                      Heat::Boundary::Neumann<PassiveFloat, 1>{0.0, 0.0},
                      Heat::TimeIntegration::RK45{});

  std::cout << "============================================================\n";

  compare_derivatives(p,
                      eps,
                      Heat::Laplace::CentralFD{},
                      Heat::Boundary::Neumann<PassiveFloat, 2>{0.0, 0.0},
                      Heat::TimeIntegration::ExplicitEuler{});

  compare_derivatives(p,
                      eps,
                      Heat::Laplace::CentralFD{},
                      Heat::Boundary::Neumann<PassiveFloat, 2>{0.0, 0.0},
                      Heat::TimeIntegration::ImplicitEuler{});

  compare_derivatives(p,
                      eps,
                      Heat::Laplace::CentralFD{},
                      Heat::Boundary::Neumann<PassiveFloat, 2>{0.0, 0.0},
                      Heat::TimeIntegration::RK4{});

  compare_derivatives(p,
                      eps,
                      Heat::Laplace::CentralFD{},
                      Heat::Boundary::Neumann<PassiveFloat, 2>{0.0, 0.0},
                      Heat::TimeIntegration::RK45{});

  std::cout << "============================================================\n";

  compare_derivatives(p,
                      eps,
                      Heat::Laplace::CentralFD4{},
                      Heat::Boundary::Neumann<PassiveFloat, 2>{0.0, 0.0},
                      Heat::TimeIntegration::ExplicitEuler{});

  compare_derivatives(p,
                      eps,
                      Heat::Laplace::CentralFD4{},
                      Heat::Boundary::Neumann<PassiveFloat, 2>{0.0, 0.0},
                      Heat::TimeIntegration::ImplicitEuler{});

  compare_derivatives(p,
                      eps,
                      Heat::Laplace::CentralFD4{},
                      Heat::Boundary::Neumann<PassiveFloat, 2>{0.0, 0.0},
                      Heat::TimeIntegration::RK4{});

  compare_derivatives(p,
                      eps,
                      Heat::Laplace::CentralFD4{},
                      Heat::Boundary::Neumann<PassiveFloat, 2>{0.0, 0.0},
                      Heat::TimeIntegration::RK45{});
}
