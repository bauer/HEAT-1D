#include <chrono>
#include <filesystem>
#include <iostream>
namespace fs = std::filesystem;

#include "BoundaryCondition.hpp"
#include "Concepts.hpp"
#include "GradientSum.hpp"
#include "Rhs.hpp"
#include "Solver.hpp"
#include "SpaceGrid.hpp"
#include "TimeIntegration.hpp"

#include "EigenToNpy.hpp"

#define OUTPUT_DIR HEAT_STRINGIFY(HEAT_DATA_OUTPUT_DIR) "run_burgers_1d/"

// -------------------------------------------------------------------------------------------------
auto main() -> int {
  using Float = double;

  constexpr Float x_min = 0.0;
  constexpr Float x_max = 1.0;
  constexpr size_t n    = 51;

#define UNIFORM_GRID

  constexpr auto grid_func = [&](Heat::Grid::Idx<1> i) {
    constexpr auto dx = (x_max - x_min) / static_cast<Float>(n - 1UL);
    const auto x      = x_min + dx * static_cast<Float>(i[0]);

#ifndef UNIFORM_GRID
    constexpr Float x_mid = (x_max + x_min) / static_cast<Float>(2);
    const auto max        = std::pow(x_mid, 3);
    return (max + std::pow(x - x_mid, 3)) / (static_cast<Float>(2) * max);
#else
    return x;
#endif  // UNIFORM_GRID
  };

  auto u0_func = [](Float x) { return x * static_cast<Float>(x <= static_cast<Float>(0.5)); };
  // auto u0_func = [](Float x) {
  //   return static_cast<Float>(0.5) * static_cast<Float>(x <= static_cast<Float>(0.5));
  // };

  {
    std::error_code ec;
    const auto _ = fs::create_directories(OUTPUT_DIR, ec);
    if (ec) {
      HEAT_WARN("Could not create directory " OUTPUT_DIR ": " << ec.message());
      return 1;
    }
  }

  Heat::Grid::OneD<Float, Float> grid{n, grid_func, u0_func};

  {
    const std::string x_output_file = OUTPUT_DIR "x0.npy";
    const std::string u_output_file = OUTPUT_DIR "u0.npy";
    if (!Heat::save_to_npy<Float>(x_output_file, u_output_file, grid)) {
      HEAT_WARN("Could not save `x0` to `" << x_output_file << "` and `u0` to `" << u_output_file
                                           << "`.");
      return 1;
    }
    HEAT_INFO("Wrote `x0` to `" << x_output_file << "`.");
    HEAT_INFO("Wrote `u0` to `" << u_output_file << "`.");
    std::cout << '\n';
  }

  // constexpr auto gradient_sum = Heat::GradientSum::CentralFD{};
  // constexpr auto gradient_sum = Heat::GradientSum::ForwardFD{};
  constexpr auto gradient_sum = Heat::GradientSum::BackwardFD{};
  // constexpr auto gradient_sum = Heat::GradientSum::BackwardFD4{};
  constexpr auto rhs = Heat::Rhs::BurgersEq{gradient_sum};

  constexpr auto boundary = Heat::Boundary::Dirichlet<Float>{0.0, 0.0};
  // constexpr auto boundary = Heat::Boundary::Neumann<Float, 2>{0.0, 0.0};

  // constexpr auto integrator = Heat::TimeIntegration::ExplicitEuler{};
  // constexpr auto integrator = Heat::TimeIntegration::ImplicitEuler{1e-8, 100'000UL};
  constexpr auto integrator = Heat::TimeIntegration::RK4{};
  // constexpr auto integrator = Heat::TimeIntegration::RK45{};

  HEAT_INFO("gradient_sum = " << gradient_sum.name());
  HEAT_INFO("boundary     = " << boundary.name());
  HEAT_INFO("integrator   = " << integrator.name());
  std::cout << '\n';

  Heat::
      Solver<Float, Float, decltype(grid), decltype(rhs), decltype(boundary), decltype(integrator)>
          solver{rhs, integrator, boundary, grid};

  constexpr Float tend = 1.0;
  const Float min_dx   = grid.min_dx();
  const Float dt       = 0.5 * 0.5 * min_dx * min_dx;

  HEAT_INFO("tend   = " << tend);
  HEAT_INFO("min_dx = " << min_dx);
  HEAT_INFO("dt     = " << dt);
  std::cout << '\n';

  const auto t_begin = std::chrono::high_resolution_clock::now();
  const auto u       = solver.solve(tend, dt);
  const auto t_dur =
      std::chrono::duration<double>(std::chrono::high_resolution_clock::now() - t_begin);
  HEAT_INFO("Solver took " << t_dur.count() << "s\n");

  {
    const auto& values = u.values();
    const auto num_nan = std::count_if(
        std::cbegin(values), std::cend(values), [](const auto& e) { return std::isnan(e); });
    HEAT_INFO("#NaN entries = " << num_nan);
  }

  {
    const std::string x_output_file = OUTPUT_DIR "x.npy";
    const std::string u_output_file = OUTPUT_DIR "u.npy";
    if (!Heat::save_to_npy<Float>(x_output_file, u_output_file, u)) {
      HEAT_WARN("Could not save `x` to `" << x_output_file << "` and `u` to `" << u_output_file
                                          << "`.");
      return 1;
    }
    HEAT_INFO("Wrote `x` to `" << x_output_file << "`.");
    HEAT_INFO("Wrote `u` to `" << u_output_file << "`.");
  }
}
