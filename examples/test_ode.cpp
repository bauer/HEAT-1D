#include <iostream>

#include "BoundaryCondition.hpp"
#include "Macros.hpp"
#include "Solver.hpp"
#include "SpaceGrid.hpp"
#include "TimeIntegration.hpp"

// -------------------------------------------------------------------------------------------------
template <typename ActiveFloat, typename PassiveFloat, typename Rhs, typename Integrator>
auto run_solver(ActiveFloat u0,
                const Rhs& rhs,
                PassiveFloat dt,
                PassiveFloat tend,
                const Integrator& integrator) -> ActiveFloat {
  Heat::Grid::ZeroD<ActiveFloat, PassiveFloat> grid{u0};

  constexpr auto boundary = Heat::Boundary::Noop<PassiveFloat>{};

  Heat::Solver<ActiveFloat,
               PassiveFloat,
               decltype(grid),
               decltype(rhs),
               decltype(boundary),
               Integrator>
      solver{rhs, integrator, boundary, grid};

  return solver.solve(tend, dt).values();
}

auto main() -> int {
  using Float = double;

  {
    constexpr auto dt   = static_cast<Float>(1e-1);
    constexpr auto tend = static_cast<Float>(1.5);
    constexpr Float u0  = 1.0;
    constexpr Float a   = 2.0;
    constexpr auto rhs  = [=]<typename ActiveFloat, typename PassiveFloat, typename SpaceGrid>(
                             const SpaceGrid& /*grid*/,
                             const SpaceGrid::Index& /*idx*/) constexpr noexcept -> ActiveFloat {
      return a;
    };

    HEAT_INFO("Model:");
    HEAT_INFO("  du/dt = " << a);
    HEAT_INFO("  u(0)  = " << u0 << '\n');

    const auto u_ex = (a * tend + u0);
    HEAT_INFO("Exact: u(" << tend << ") = " << u_ex << '\n');

    HEAT_INFO("dt = " << dt << '\n');

    {
      auto u = run_solver(u0, rhs, dt, tend, Heat::TimeIntegration::ExplicitEuler{});
      HEAT_INFO("Explicit Euler: u(" << tend << ") = " << u
                                     << " (relative error = " << ((u - u_ex) / u_ex) << ")");
    }

    {
      auto u = run_solver(u0, rhs, dt, tend, Heat::TimeIntegration::ImplicitEuler{});
      HEAT_INFO("Implicit Euler: u(" << tend << ") = " << u
                                     << " (relative error = " << ((u - u_ex) / u_ex) << ")");
    }

    {
      auto u = run_solver(u0, rhs, dt, tend, Heat::TimeIntegration::RK4{});
      HEAT_INFO("RK4:            u(" << tend << ") = " << u
                                     << " (relative error = " << ((u - u_ex) / u_ex) << ")");
    }

    {
      auto u = run_solver(u0, rhs, dt, tend, Heat::TimeIntegration::RK45{1e-12});
      HEAT_INFO("RK4(5):         u(" << tend << ") = " << u
                                     << " (relative error = " << ((u - u_ex) / u_ex) << ")");
    }
  }

  std::cout << "--------------------------------------------------------------------------------\n";

  {
    constexpr auto dt   = static_cast<Float>(5e-2);
    constexpr auto tend = static_cast<Float>(1.5);
    constexpr Float u0  = 1.0;
    constexpr Float a   = 10.0;
    constexpr auto rhs  = [=]<typename ActiveFloat, typename PassiveFloat, typename SpaceGrid>(
                             const SpaceGrid& grid,
                             const SpaceGrid::Index& idx) constexpr noexcept -> ActiveFloat {
      return a * grid.value_at(idx);
    };

    HEAT_INFO("Model:");
    HEAT_INFO("  du/dt = " << a << "*u");
    HEAT_INFO("  u(0)  = " << u0 << '\n');

    const auto u_ex = std::exp(a * tend + std::log(u0));
    HEAT_INFO("Exact: u(" << tend << ") = " << u_ex << '\n');

    HEAT_INFO("dt = " << dt << '\n');
    {
      auto u = run_solver(u0, rhs, dt, tend, Heat::TimeIntegration::ExplicitEuler{});
      HEAT_INFO("Explicit Euler: u(" << tend << ") = " << u
                                     << " (relative error = " << ((u - u_ex) / u_ex) << ")");
    }

    {
      auto u = run_solver(u0, rhs, dt, tend, Heat::TimeIntegration::ImplicitEuler{});
      HEAT_INFO("Implicit Euler: u(" << tend << ") = " << u
                                     << " (relative error = " << ((u - u_ex) / u_ex) << ")");
    }

    {
      auto u = run_solver(u0, rhs, dt, tend, Heat::TimeIntegration::RK4{});
      HEAT_INFO("RK4:            u(" << tend << ") = " << u
                                     << " (relative error = " << ((u - u_ex) / u_ex) << ")");
    }

    {
      auto u = run_solver(u0, rhs, dt, tend, Heat::TimeIntegration::RK45{1e-12});
      HEAT_INFO("RK4(5):         u(" << tend << ") = " << u
                                     << " (relative error = " << ((u - u_ex) / u_ex) << ")");
    }
  }

  std::cout << "--------------------------------------------------------------------------------\n";

  {
    constexpr auto dt   = static_cast<Float>(1e-6);
    constexpr auto tend = static_cast<Float>(1.5);
    constexpr auto u0   = static_cast<Float>(1.0);
    constexpr auto a    = static_cast<Float>(0.5);
    constexpr auto rhs  = [=]<typename ActiveFloat, typename PassiveFloat, typename SpaceGrid>(
                             const SpaceGrid& grid,
                             const SpaceGrid::Index& idx) constexpr noexcept -> ActiveFloat {
      return a * grid.value_at(idx) * grid.value_at(idx);
    };

    HEAT_INFO("Model:");
    HEAT_INFO("  du/dt = " << a << "*u^2");
    HEAT_INFO("  u(0)  = " << u0 << '\n');

    const auto u_ex = static_cast<Float>(1) / (static_cast<Float>(1) - a * tend);
    HEAT_INFO("Exact: u(" << tend << ") = " << u_ex << '\n');

    HEAT_INFO("dt = " << dt << '\n');
    {
      auto u = run_solver(u0, rhs, dt, tend, Heat::TimeIntegration::ExplicitEuler{});
      HEAT_INFO("Explicit Euler: u(" << tend << ") = " << u
                                     << " (relative error = " << ((u - u_ex) / u_ex) << ")");
    }

    {
      auto u = run_solver(u0, rhs, dt, tend, Heat::TimeIntegration::ImplicitEuler{});
      HEAT_INFO("Implicit Euler: u(" << tend << ") = " << u
                                     << " (relative error = " << ((u - u_ex) / u_ex) << ")");
    }

    {
      auto u = run_solver(u0, rhs, dt, tend, Heat::TimeIntegration::RK4{});
      HEAT_INFO("RK4:            u(" << tend << ") = " << u
                                     << " (relative error = " << ((u - u_ex) / u_ex) << ")");
    }

    {
      auto u = run_solver(u0, rhs, dt, tend, Heat::TimeIntegration::RK45{1e-12});
      HEAT_INFO("RK4(5):         u(" << tend << ") = " << u
                                     << " (relative error = " << ((u - u_ex) / u_ex) << ")");
    }
  }

  std::cout << "--------------------------------------------------------------------------------\n";

  {
    constexpr auto dt   = static_cast<Float>(1e-1);
    constexpr auto tend = static_cast<Float>(1.5);
    constexpr auto u0   = static_cast<Float>(1.0);
    constexpr auto a    = static_cast<Float>(0.5);
    constexpr auto rhs  = [=]<typename ActiveFloat, typename PassiveFloat, typename SpaceGrid>(
                             const SpaceGrid& grid,
                             const SpaceGrid::Index& idx) constexpr noexcept -> ActiveFloat {
      return a * std::sin(grid.value_at(idx));
    };

    HEAT_INFO("Model:");
    HEAT_INFO("  du/dt = " << a << " * sin(u)");
    HEAT_INFO("  u(0)  = " << u0 << '\n');

    const auto u_ex = 1.7157;
    HEAT_INFO("Exact: u(" << tend << ") = " << u_ex << '\n');

    HEAT_INFO("dt = " << dt << '\n');
    {
      auto u = run_solver(u0, rhs, dt, tend, Heat::TimeIntegration::ExplicitEuler{});
      HEAT_INFO("Explicit Euler: u(" << tend << ") = " << u
                                     << " (relative error = " << ((u - u_ex) / u_ex) << ")");
    }

    {
      auto u = run_solver(u0, rhs, dt, tend, Heat::TimeIntegration::ImplicitEuler{});
      HEAT_INFO("Implicit Euler: u(" << tend << ") = " << u
                                     << " (relative error = " << ((u - u_ex) / u_ex) << ")");
    }

    {
      auto u = run_solver(u0, rhs, dt, tend, Heat::TimeIntegration::RK4{});
      HEAT_INFO("RK4:            u(" << tend << ") = " << u
                                     << " (relative error = " << ((u - u_ex) / u_ex) << ")");
    }

    {
      auto u = run_solver(u0, rhs, dt, tend, Heat::TimeIntegration::RK45{1e-12});
      HEAT_INFO("RK4(5):         u(" << tend << ") = " << u
                                     << " (relative error = " << ((u - u_ex) / u_ex) << ")");
    }
  }

  std::cout << "--------------------------------------------------------------------------------\n";

  {
    using PassiveFloat = Float;
    using ActiveFloat  = Eigen::Vector<PassiveFloat, 2>;

    constexpr auto tend = static_cast<PassiveFloat>(2.0);
    constexpr auto dt   = static_cast<PassiveFloat>(0.1);
    const auto u0       = ActiveFloat{0.5, 1.5};

    constexpr auto rhs = []<typename ActiveFloat, typename PassiveFloat, typename SpaceGrid>(
                             const SpaceGrid& grid,
                             const SpaceGrid::Index& idx) constexpr noexcept -> ActiveFloat {
      const auto& u = grid.value_at(idx);
      return ActiveFloat{u(1), -u(0)};
    };

    HEAT_INFO("Model:");
    HEAT_INFO("  d^2u/dt^2 + u = 0");
    HEAT_INFO("  u(0)  = " << u0(0));
    HEAT_INFO("  u'(0) = " << u0(1));
    std::cout << '\n';

    const auto u_expected = ActiveFloat{
        u0(1) * std::sin(tend) + u0(0) * std::cos(tend),
        u0(1) * std::cos(tend) - u0(0) * std::sin(tend),
    };

    HEAT_INFO("Expected:");
    HEAT_INFO("  u(" << tend << ")  = " << u_expected(0));
    HEAT_INFO("  u'(" << tend << ") = " << u_expected(1));
    std::cout << '\n';

    HEAT_INFO("dt = " << dt << '\n');

    {
      const auto u = run_solver(u0, rhs, dt, tend, Heat::TimeIntegration::ExplicitEuler{});
      HEAT_INFO("ExplicitEuler:");
      HEAT_INFO("  u(" << tend << ")  = " << u(0) << " (relative error = "
                       << std::abs(u(0) - u_expected(0)) / u_expected(0) << ")");
      HEAT_INFO("  u'(" << tend << ") = " << u(1) << " (relative error = "
                        << std::abs(u(1) - u_expected(1)) / u_expected(1) << ")");
    }

    {
      const auto u = run_solver(u0, rhs, dt, tend, Heat::TimeIntegration::RK4{});
      HEAT_INFO("RK4:");
      HEAT_INFO("  u(" << tend << ")  = " << u(0) << " (relative error = "
                       << std::abs(u(0) - u_expected(0)) / u_expected(0) << ")");
      HEAT_INFO("  u'(" << tend << ") = " << u(1) << " (relative error = "
                        << std::abs(u(1) - u_expected(1)) / u_expected(1) << ")");
    }

    {
      const auto u = run_solver(u0, rhs, dt, tend, Heat::TimeIntegration::RK45{1e-12});
      HEAT_INFO("RK4(5):");
      HEAT_INFO("  u(" << tend << ")  = " << u(0) << " (relative error = "
                       << std::abs(u(0) - u_expected(0)) / u_expected(0) << ")");
      HEAT_INFO("  u'(" << tend << ") = " << u(1) << " (relative error = "
                        << std::abs(u(1) - u_expected(1)) / u_expected(1) << ")");
    }
  }
}
