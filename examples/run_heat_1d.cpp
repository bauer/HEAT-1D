#include <chrono>
#include <filesystem>
#include <iostream>
#include <numbers>
namespace fs = std::filesystem;

#include "BoundaryCondition.hpp"
#include "Concepts.hpp"
#include "EigenToNpy.hpp"
#include "Laplace.hpp"
#include "Rhs.hpp"
#include "Solver.hpp"
#include "SpaceGrid.hpp"
#include "TimeIntegration.hpp"

#define OUTPUT_DIR HEAT_STRINGIFY(HEAT_DATA_OUTPUT_DIR) "run_heat_1d/"

// - Run the solver and save results ---------------------------------------------------------------
template <typename Float, typename Rhs, typename Integrator, typename Boundary, typename SpaceGrid>
[[nodiscard]] constexpr auto run(Rhs rhs,
                                 Integrator integrator,
                                 Boundary boundary,
                                 const SpaceGrid& x,
                                 Float tend,
                                 Float dt,
                                 const std::string& x_output_file,
                                 const std::string& u_output_file) noexcept -> bool {
  Heat::Solver<Float, Float, SpaceGrid, Rhs, Boundary, Integrator> solver(
      rhs, integrator, boundary, x);

  const auto t_begin = std::chrono::high_resolution_clock::now();
  const auto u       = solver.solve(tend, dt);
  const auto t_dur =
      std::chrono::duration<double>(std::chrono::high_resolution_clock::now() - t_begin);
  HEAT_INFO("Solver took " << t_dur.count() << "s");

  {
    if (!Heat::save_to_npy<Float>(x_output_file, u_output_file, u)) {
      HEAT_WARN("Could not save `x` to `" << x_output_file << "` and `u` to `" << u_output_file
                                          << "`.");
      return false;
    }
    HEAT_INFO("Wrote `x` to `" << x_output_file << "`.");
    HEAT_INFO("Wrote `u` to `" << u_output_file << "`.");
  }

  {
    const auto& solution = u.values();
    if (std::any_of(std::cbegin(solution), std::cend(solution), [](const auto& ui) {
          return std::isnan(ui);
        })) {
      HEAT_WARN("u has NaN values");
    }
    if (std::any_of(std::cbegin(solution), std::cend(solution), [](const auto& ui) {
          return std::isinf(ui);
        })) {
      HEAT_WARN("u has inf values");
    }
  }

  return true;
}

auto main() -> int {
  using Float = double;

  constexpr Float x_min = 0.0;
  constexpr Float x_max = 1.0;
  constexpr size_t n    = 100;

  constexpr auto grid_func = [&](Heat::Grid::Idx<1> i) {
    constexpr auto dx = (x_max - x_min) / static_cast<Float>(n - 1UL);
    const auto x      = x_min + dx * static_cast<Float>(i[0]);

    // return std::pow(x, 2);
    return std::sqrt(x);
    // return x;
  };

  auto u0_func = [](Float x) {
    return static_cast<Float>(100) * static_cast<Float>(x <= static_cast<Float>(0.5));

    // return static_cast<Float>(100) * static_cast<Float>(x <= static_cast<Float>(1.0 / 3.0) ||
    //                                                     x >= static_cast<Float>(2.0 / 3.0));

    // return static_cast<Float>(50) *
    //        (std::cos(std::numbers::pi_v<Float> * x) + static_cast<Float>(1));

    // return std::exp(static_cast<Float>(-0.5) *
    //                 std::pow((x - static_cast<Float>(0.5)) / static_cast<Float>(0.1), 2));
  };

  {
    std::error_code ec;
    const auto _ = fs::create_directories(OUTPUT_DIR, ec);
    if (ec) {
      HEAT_WARN("Could not create directory " OUTPUT_DIR ": " << ec.message());
      return 1;
    }
  }

  const auto x_grid = Heat::Grid::OneD<Float, Float>(n, grid_func, u0_func);
  {
    constexpr auto x_output_file = OUTPUT_DIR "x.npy";
    constexpr auto u_output_file = OUTPUT_DIR "u0.npy";
    if (!Heat::save_to_npy(x_output_file, u_output_file, x_grid)) {
      HEAT_WARN("Could not save `x` to `" << x_output_file << "` and `u0` to `" << u_output_file
                                          << "`.");
      return 1;
    }
    HEAT_INFO("Wrote `x` to `" << x_output_file << "`.");
    HEAT_INFO("Wrote `u0` to `" << u_output_file << "`.");
  }

  constexpr Float a    = 0.5;
  const auto min_dx    = x_grid.min_dx();
  const auto dt        = 0.5 * min_dx * min_dx * min_dx / a;
  constexpr Float tend = 0.25;

  HEAT_INFO("a      = " << a);
  HEAT_INFO("tend   = " << tend);
  HEAT_INFO("min_dx = " << min_dx);
  HEAT_INFO("dt     = " << dt);

  // constexpr auto laplace = Heat::Laplace::CentralFD{};
  // constexpr auto laplace = Heat::Laplace::CentralFD4{};
  constexpr auto laplace = Heat::Laplace::NonUniformCentralFD{};

  constexpr auto rhs = Heat::Rhs::HeatEq{a, laplace};

  // constexpr auto boundary = Heat::Boundary::Dirichlet<Float>{100.0, 0.0};
  constexpr auto boundary = Heat::Boundary::Neumann<Float, 1UL>{0.0, 0.0};

  std::cout << '\n';
  HEAT_INFO("Run Explicit Euler...");
  if (!run(rhs,
           Heat::TimeIntegration::ExplicitEuler{},
           boundary,
           x_grid,
           tend,
           dt,
           OUTPUT_DIR "explicit_euler_x.npy",
           OUTPUT_DIR "explicit_euler_u.npy")) {
    return 1;
  }

  std::cout << '\n';
  HEAT_INFO("Run Implicit Euler...");
  if (!run(rhs,
           Heat::TimeIntegration::ImplicitEuler{1e-12, 100'000UL},
           boundary,
           x_grid,
           tend,
           dt,
           OUTPUT_DIR "implicit_euler_x.npy",
           OUTPUT_DIR "implicit_euler_u.npy")) {
    return 1;
  }

  std::cout << '\n';
  HEAT_INFO("Run Runge Kutta 4 scheme...");
  if (!run(rhs,
           Heat::TimeIntegration::RK4{},
           boundary,
           x_grid,
           tend,
           dt,
           OUTPUT_DIR "rk4_x.npy",
           OUTPUT_DIR "rk4_u.npy")) {
    return 1;
  }

  std::cout << '\n';
  HEAT_INFO("Run Runge Kutta 4(5) scheme...");
  if (!run(rhs,
           Heat::TimeIntegration::RK45{1e-12},
           boundary,
           x_grid,
           tend,
           dt,
           OUTPUT_DIR "rk45_x.npy",
           OUTPUT_DIR "rk45_u.npy")) {
    return 1;
  }
}
