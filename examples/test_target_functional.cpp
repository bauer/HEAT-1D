#include <iostream>
#include <numbers>

#include "Heat.hpp"

// - Setup -----------------------------------------------------------------------------------------
using Float = double;

constexpr auto a = static_cast<Float>(1.0);

constexpr size_t n   = 10;
constexpr auto x_min = static_cast<Float>(0.0);
constexpr auto x_max = static_cast<Float>(10.0);
constexpr auto dx    = (x_max - x_min) / static_cast<Float>(n - 1);

constexpr auto t_end = static_cast<Float>(0.5);
constexpr auto dt    = static_cast<Float>(0.5) * dx * dx / a;

// - One possible analytical solution for Neumann boundary conditions with derivative zero ---------
[[nodiscard]] constexpr auto analytical_solution(Float x, Float t) noexcept -> Float {
  constexpr std::array z = {static_cast<Float>(1.0), static_cast<Float>(3.0)};

  auto res = static_cast<Float>(0);
  for (auto zi : z) {
    res += static_cast<Float>(
        std::cos((zi * std::numbers::pi_v<Float>) / (x_max - x_min) * x) *
        std::exp(-a * std::pow((zi * std::numbers::pi_v<Float>) / (x_max - x_min), 2) * t));
  }
  return res;
}

// - Main ------------------------------------------------------------------------------------------
auto main() -> int {
  constexpr auto grid_func = [](Heat::Grid::Idx<1> i) {
    return x_min + dx * static_cast<Float>(i[0]);
  };

  constexpr auto u0_func = [](Float x) { return analytical_solution(x, static_cast<Float>(0)); };

  Heat::Grid::OneD<Float, Float> grid(n, grid_func, u0_func);

  Heat::Laplace::CentralFD4 laplace{};
  Heat::Rhs::HeatEq rhs{a, laplace};

  // Heat::Boundary::Neumann<Float, 1> boundary{0.0, 0.0};
  Heat::Boundary::Neumann<Float, 2> boundary{0.0, 0.0};

  // Heat::TimeIntegration::ExplicitEuler integrator{};
  // Heat::TimeIntegration::ImplicitEuler integrator{};
  Heat::TimeIntegration::RK4 integrator{};
  // Heat::TimeIntegration::RK45 integrator{};

  HEAT_INFO("Laplace:    " << laplace.name());
  HEAT_INFO("Boundary:   " << boundary.name());
  HEAT_INFO("Integrator: " << integrator.name());

  Heat::
      Solver<Float, Float, decltype(grid), decltype(rhs), decltype(boundary), decltype(integrator)>
          solver{rhs, integrator, boundary, grid};

  auto F = [t = static_cast<Float>(0)](const Heat::Grid::OneD<Float, Float>& u,
                                       Float dt,
                                       Eigen::Vector<Float, 4> prev_value) mutable {
    t += dt;
    constexpr std::array<Float, 4> sensor_pos = {
        x_min, 0.33 * (x_max - x_min) + x_min, 0.66 * (x_max - x_min) + x_min, x_max};

    for (int i = 0; i < 4; ++i) {
      // NOLINTBEGIN(cppcoreguidelines-pro-bounds-constant-array-index)
      const auto actual    = analytical_solution(sensor_pos[static_cast<size_t>(i)], t);
      const auto numerical = u.eval_at(sensor_pos[static_cast<size_t>(i)]);
      // NOLINTEND(cppcoreguidelines-pro-bounds-constant-array-index)
      prev_value(i) += std::pow(actual - numerical, 2) * dt;
    }

    return prev_value;
  };
  Eigen::Vector<Float, 4> functional_value = Eigen::Vector<Float, 4>::Zero();

  const auto u = solver.solve_and_evaluate(t_end, dt, F, functional_value);

  HEAT_INFO("functional_value = " << functional_value.transpose());
}
