#include <chrono>
#include <filesystem>
#include <iostream>
namespace fs = std::filesystem;

#include "BaselineSolver.hpp"

auto main() -> int {
  using Float = double;

  constexpr int64_t n = 1000;
  Eigen::VectorX<Float> u0(n);
  for (Eigen::Index i = 0; i < n; ++i) {
    // u0(i) = std::lerp(100.0, 0.0, static_cast<Float>(i) / static_cast<Float>(n - 1));
    u0(i) = 0.0;
  }
  u0(0)     = 0.0;
  u0(n - 1) = 100.0;

  constexpr Float a = 0.5;

  constexpr Float tend = 0.5;

  constexpr Float dx = 1.0 / static_cast<Float>(n);

  {
    constexpr auto OUTPUT_DIR = HEAT_STRINGIFY(HEAT_DATA_OUTPUT_DIR) "compare_baseline/";
    std::error_code ec;
    const auto _ = fs::create_directories(OUTPUT_DIR, ec);
    if (ec) {
      HEAT_WARN("Could not create directory " << OUTPUT_DIR << ": " << ec.message());
      return 1;
    }
  }

  {
    HEAT_INFO("Explicit matrix free:");

    constexpr Float dt = 0.5 * dx * dx / a;
    HEAT_INFO("dx = " << dx);
    HEAT_INFO("dt = " << dt);

    constexpr auto u_file =
        HEAT_STRINGIFY(HEAT_DATA_OUTPUT_DIR) "compare_baseline/u_explicit_matrix_free.data";
    std::optional<std::ofstream> u_out(std::in_place, u_file, std::ios::out | std::ios::binary);
    if (!u_out.has_value() && !(*u_out)) {
      HEAT_WARN("Could not open file " << u_file << ": " << std::strerror(errno));
      return 1;
    }
    if (!u_out->write(reinterpret_cast<const char*>(&n), sizeof(n))) {  // NOLINT
      HEAT_WARN("Could not write n to " << u_file << ": " << std::strerror(errno));
      return 1;
    }

    const auto t_begin = std::chrono::high_resolution_clock::now();
    const auto opt_res =
        Heat::solve(u0, a, tend, dx, dt, u_out, Heat::SolverMode::ExplicitMatrixFree);
    const auto t_dur =
        std::chrono::duration<double>(std::chrono::high_resolution_clock::now() - t_begin);

    if (!opt_res.has_value()) {
      HEAT_WARN("Solving heat equation failed.");
      return 1;
    }

    HEAT_INFO("Solving heat equation took " << t_dur.count() << "s");
    HEAT_INFO("Wrote results to " << u_file);
  }

  std::cout << "------------------------------------------------------------\n";

  {
    HEAT_INFO("Explicit:");

    constexpr Float dt = 0.5 * dx * dx / a;
    HEAT_INFO("dx = " << dx);
    HEAT_INFO("dt = " << dt);

    constexpr auto u_file = HEAT_STRINGIFY(HEAT_DATA_OUTPUT_DIR) "compare_baseline/u_explicit.data";
    std::optional<std::ofstream> u_out(std::in_place, u_file, std::ios::out | std::ios::binary);
    if (!u_out.has_value() && !(*u_out)) {
      HEAT_WARN("Could not open file " << u_file << ": " << std::strerror(errno));
      return 1;
    }
    if (!u_out->write(reinterpret_cast<const char*>(&n), sizeof(n))) {  // NOLINT
      HEAT_WARN("Could not write n to " << u_file << ": " << std::strerror(errno));
      return 1;
    }

    const auto t_begin = std::chrono::high_resolution_clock::now();
    const auto opt_res = Heat::solve(u0, a, tend, dx, dt, u_out, Heat::SolverMode::Explicit);
    const auto t_dur =
        std::chrono::duration<double>(std::chrono::high_resolution_clock::now() - t_begin);

    if (!opt_res.has_value()) {
      HEAT_WARN("Solving heat equation failed.");
      return 1;
    }

    HEAT_INFO("Solving heat equation took " << t_dur.count() << "s");
    HEAT_INFO("Wrote results to " << u_file);
  }

  std::cout << "------------------------------------------------------------\n";

  {
    HEAT_INFO("Implicit:");

    constexpr Float dt = dx;
    HEAT_INFO("dx = " << dx);
    HEAT_INFO("dt = " << dt);

    constexpr auto u_file = HEAT_STRINGIFY(HEAT_DATA_OUTPUT_DIR) "compare_baseline/u_implicit.data";
    std::optional<std::ofstream> u_out(std::in_place, u_file, std::ios::out | std::ios::binary);
    if (!u_out.has_value() && !(*u_out)) {
      HEAT_WARN("Could not open file " << u_file << ": " << std::strerror(errno));
      return 1;
    }
    if (!u_out->write(reinterpret_cast<const char*>(&n), sizeof(n))) {  // NOLINT
      HEAT_WARN("Could not write n to " << u_file << ": " << std::strerror(errno));
      return 1;
    }

    const auto t_begin = std::chrono::high_resolution_clock::now();
    const auto opt_res = Heat::solve(u0, a, tend, dx, dt, u_out, Heat::SolverMode::Implicit);
    const auto t_dur =
        std::chrono::duration<double>(std::chrono::high_resolution_clock::now() - t_begin);

    if (!opt_res.has_value()) {
      HEAT_WARN("Solving heat equation failed.");
      return 1;
    }

    HEAT_INFO("Solving heat equation took " << t_dur.count() << "s");
    HEAT_INFO("Wrote results to " << u_file);
  }
}
