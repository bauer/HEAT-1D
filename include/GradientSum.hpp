#ifndef HEAT_GRADIENT_SUM_HPP_
#define HEAT_GRADIENT_SUM_HPP_

#include "Concepts.hpp"

namespace Heat::GradientSum {

namespace detail {

constexpr auto id_func = []<typename Float>(const Float& u) constexpr noexcept -> Float {
  return u;
};

}  // namespace detail

/**
 * @class ForwardFD
 * @brief Numerical approximation of the sum over the gradient using forward finite differences of
 * first order.
 *
 * ForwardFD approximates the value of function \f$\sum_{i=1}^{d} \frac{\partial}{\partial x_i}
 * f(u(x))\f$ for a given function \f$f\f$ and the the current solution of the PDE \f$u\f$.
 */
class ForwardFD {
 public:
  /**
   * @brief Evaluate the sum over the gradient at a grid position.
   * @tparam ActiveFloat Type for the active floating point type, e.g. double or an AD type.
   * @tparam PassiveFloat Type for the passive floating point type, usually double or float.
   * @tparam SpaceGrid Type for the grid that forms the discretization in space.
   * @tparam FUNC Type of the function \f$f\f$
   * @param  grid SpaceGrid containing the space discretization and the value of the function at the
   * respective points.
   * @param idx Index into the grid at which the gradient sum will be approximated.
   * @param[optional] f Function \f$f\f$, default is \f$f(x) = x\f$
   * @return Approximation of the gradient sum at grid position
   */
  template <typename ActiveFloat,
            typename PassiveFloat = ActiveFloat,
            SpaceGrid_c<ActiveFloat, PassiveFloat> SpaceGrid,
            typename FUNC = decltype(detail::id_func)>
  [[nodiscard]] constexpr auto
  operator()(const SpaceGrid& grid,
             const typename SpaceGrid::Index& idx,
             const FUNC& f = detail::id_func) const noexcept -> ActiveFloat {
    static_assert(SpaceGrid::DIM == 1UL, "Higher dimension than DIM = 1 not implemented yet.");
    assert(idx.is_interior(grid.size()));
    const auto dx = grid.x_at(idx.next(0)) - grid.x_at(idx);
    return (f(grid.value_at(idx.next(0))) - f(grid.value_at(idx))) / dx;
  }

  /** @return Name of the approximation method */
  [[nodiscard]] static constexpr auto name() noexcept { return "ForwardFD"; }
};

/**
 * @class BackwardFD
 * @brief Numerical approximation of the sum over the gradient using backward finite differences of
 * first order.
 *
 * BackwardFD approximates the value of function \f$\sum_{i=1}^{d} \frac{\partial}{\partial x_i}
 * f(u(x))\f$ for a given function \f$f\f$ and the the current solution of the PDE \f$u\f$.
 */
class BackwardFD {
 public:
  /**
   * @brief Evaluate the sum over the gradient at a grid position.
   * @tparam ActiveFloat Type for the active floating point type, e.g. double or an AD type.
   * @tparam PassiveFloat Type for the passive floating point type, usually double or float.
   * @tparam SpaceGrid Type for the grid that forms the discretization in space.
   * @tparam FUNC Type of the function \f$f\f$
   * @param grid SpaceGrid containing the space discretization and the value of the function at the
   * respective points.
   * @param idx Index into the grid at which the gradient sum will be approximated.
   * @param[optional] f Function \f$f\f$, default is \f$f(x) = x\f$
   * @return Approximation of the gradient sum at grid position
   */
  template <typename ActiveFloat,
            typename PassiveFloat = ActiveFloat,
            SpaceGrid_c<ActiveFloat, PassiveFloat> SpaceGrid,
            typename FUNC = decltype(detail::id_func)>
  [[nodiscard]] constexpr auto
  operator()(const SpaceGrid& grid,
             const typename SpaceGrid::Index& idx,
             const FUNC& f = detail::id_func) const noexcept -> ActiveFloat {
    static_assert(SpaceGrid::DIM == 1UL, "Higher dimension than DIM = 1 not implemented yet.");
    assert(idx.is_interior(grid.size()));
    const auto dx = grid.x_at(idx) - grid.x_at(idx.prev(0));
    return (f(grid.value_at(idx)) - f(grid.value_at(idx.prev(0)))) / dx;
  }

  /** @return Name of the approximation method */
  [[nodiscard]] static constexpr auto name() noexcept { return "BackwardFD"; }
};

/**
 * @class BackwardFD4
 * @brief Numerical approximation of the sum over the gradient using backward finite differences of
 * fourth order.
 *
 * BackwardFD4 approximates the value of function \f$\sum_{i=1}^{d} \frac{\partial}{\partial x_i}
 * f(u(x))\f$ for a given function \f$f\f$ and the the current solution of the PDE \f$u\f$.
 *
 * BackwardFD4 requires the grid to be uniform.
 */
class BackwardFD4 {
 public:
  /**
   * @brief Evaluate the sum over the gradient at a grid position.
   * @tparam ActiveFloat Type for the active floating point type, e.g. double or an AD type.
   * @tparam PassiveFloat Type for the passive floating point type, usually double or float.
   * @tparam SpaceGrid Type for the grid that forms the discretization in space.
   * @tparam FUNC Type of the function \f$f\f$
   * @param grid SpaceGrid containing the space discretization and the value of the function at the
   * respective points.
   * @param idx Index into the grid at which the gradient sum will be approximated.
   * @param[optional] f Function \f$f\f$, default is \f$f(x) = x\f$
   * @return Approximation of the gradient sum at grid position
   */
  template <typename ActiveFloat,
            typename PassiveFloat = ActiveFloat,
            SpaceGrid_c<ActiveFloat, PassiveFloat> SpaceGrid,
            typename FUNC = decltype(detail::id_func)>
  [[nodiscard]] constexpr auto
  operator()(const SpaceGrid& grid,
             const typename SpaceGrid::Index& idx,
             const FUNC& f = detail::id_func) const noexcept -> ActiveFloat {
    static_assert(SpaceGrid::DIM == 1UL, "Higher dimension than DIM = 1 not implemented yet.");
    assert(idx.is_interior(grid.size()));
    const auto dx = grid.x_at(idx) - grid.x_at(idx.prev(0));

    if (idx[0] >= 4) {
      constexpr auto C0 = static_cast<PassiveFloat>(25);
      constexpr auto C1 = static_cast<PassiveFloat>(-48);
      constexpr auto C2 = static_cast<PassiveFloat>(36);
      constexpr auto C3 = static_cast<PassiveFloat>(-16);
      constexpr auto C4 = static_cast<PassiveFloat>(3);

      assert(grid.size()[0] >= 4);
      return (C0 * f(grid.value_at(idx)) +                                //
              C1 * f(grid.value_at(idx.prev(0))) +                        //
              C2 * f(grid.value_at(idx.prev(0).prev(0))) +                //
              C3 * f(grid.value_at(idx.prev(0).prev(0).prev(0))) +        //
              C4 * f(grid.value_at(idx.prev(0).prev(0).prev(0).prev(0)))  //
              ) /
             (static_cast<PassiveFloat>(12) * dx);
    }

    // Fall back to first order
    return (f(grid.value_at(idx)) - f(grid.value_at(idx.prev(0)))) / dx;
  }

  /** @return Name of the approximation method */
  [[nodiscard]] static constexpr auto name() noexcept { return "BackwardFD4"; }
};

/**
 * @class CentralFD
 * @brief Numerical approximation of the sum over the gradient using central finite differences of
 * second order.
 *
 * CentralFD approximates the value of function \f$\sum_{i=1}^{d} \frac{\partial}{\partial x_i}
 * f(u(x))\f$ for a given function \f$f\f$ and the the current solution of the PDE \f$u\f$.
 */
class CentralFD {
 public:
  /**
   * @brief Evaluate the sum over the gradient at a grid position.
   * @tparam ActiveFloat Type for the active floating point type, e.g. double or an AD type.
   * @tparam PassiveFloat Type for the passive floating point type, usually double or float.
   * @tparam SpaceGrid Type for the grid that forms the discretization in space.
   * @tparam FUNC Type of the function \f$f\f$
   * @param grid SpaceGrid containing the space discretization and the value of the function at the
   * respective points.
   * @param idx Index into the grid at which the gradient sum will be approximated.
   * @param[optional] f Function \f$f\f$, default is \f$f(x) = x\f$
   * @return Approximation of the gradient sum at grid position
   */
  template <typename ActiveFloat,
            typename PassiveFloat = ActiveFloat,
            SpaceGrid_c<ActiveFloat, PassiveFloat> SpaceGrid,
            typename FUNC = decltype(detail::id_func)>
  [[nodiscard]] constexpr auto
  operator()(const SpaceGrid& grid,
             const typename SpaceGrid::Index& idx,
             const FUNC& f = detail::id_func) const noexcept -> ActiveFloat {
    static_assert(SpaceGrid::DIM == 1UL, "Higher dimension than DIM = 1 not implemented yet.");
    assert(idx.is_interior(grid.size()));
    const auto dx = grid.x_at(idx.next(0)) - grid.x_at(idx);
    return (f(grid.value_at(idx.next(0))) - f(grid.value_at(idx.prev(0)))) /
           (static_cast<PassiveFloat>(2) * dx);
  }

  /** @return Name of the approximation method */
  [[nodiscard]] static constexpr auto name() noexcept { return "CentralFD"; }
};

}  // namespace Heat::GradientSum

#endif  // HEAT_GRADIENT_SUM_HPP_
