#ifndef HEAT_GRID_INDEX_HPP_
#define HEAT_GRID_INDEX_HPP_

#include <array>
#include <cassert>

#include "Macros.hpp"

namespace Heat::Grid {

// - Size of an DIM-dimensional grid ---------------------------------------------------------------
/**
 * @class Size
 * @brief Size of an n-dimensional space grid.
 *
 * Size holds the number of grid points in each dimension in a static array.
 *
 * @tparam DIM Number of dimensions.
 */
template <size_t DIM>
struct Size {
  /// Number of grid points in each respective dimension.
  std::array<size_t, DIM> size;

  /**
   * @brief Number of grid points in a given dimension
   * @param i Dimension
   * @return Number of grid points
   */
  [[nodiscard]] constexpr auto operator[](size_t i) const noexcept -> size_t {
    assert(i < DIM);
    return size[i];  // NOLINT
  }

  /** @brief Default comparison operator */
  [[nodiscard]] constexpr auto operator==(const Size<DIM>& other) const noexcept -> bool = default;
};

// - Index into an DIM-dimensional grid ------------------------------------------------------------
/**
 * @class Idx
 * @brief Index into a n-dimensional space-grid
 *
 * Idx allows to index into any n-dimensional space-grid and advance the index either to the next
 * interior or boundary point.
 *
 * @tparam DIM Number of dimensions.
 */
template <size_t DIM>
struct Idx {
  /// Index into the space-grid
  std::array<size_t, DIM> idx;

  /// Access the i-th element of the index
  [[nodiscard]] constexpr auto operator[](size_t i) noexcept -> size_t& {
    assert(i < DIM);
    return idx[i];  // NOLINT
  }

  /// Access the i-th element of the index
  [[nodiscard]] constexpr auto operator[](size_t i) const noexcept -> size_t {
    assert(i < DIM);
    return idx[i];  // NOLINT
  }

  /**
   * @brief Advance the current index to the next interior point inplace.
   * @param size Size of the corresponding space-grid, needed to find the next interior point in
   * dimensions greater than one.
   */
  constexpr void advance_interior([[maybe_unused]] const Size<DIM>& size) noexcept {
    if constexpr (DIM == 1) {
      idx[0] += 1;
    } else {
      HEAT_PANIC("Not implemented for DIM=" << DIM << " yet.");
    }
  }

  /**
   * @brief Advance the current index to the next boundary point inplace.
   * @param size Size of the corresponding space-grid, needed to find the next boundary point in
   * dimensions greater than zero.
   */
  constexpr void advance_boundary([[maybe_unused]] const Size<DIM>& size) noexcept {
    if constexpr (DIM == 1) {
      if (idx[0] == 0UL) {
        idx[0] = size[0] - 1UL;
      } else {
        idx[0] += 1UL;
      }
    } else {
      HEAT_PANIC("Not implemented for DIM=" << DIM << " yet.");
    }
  }

  /**
   * @brief Get the next index in a given dimension.
   * @param axis Dimension in which the index should be advanced.
   * @return Next index in the given dimension.
   */
  [[nodiscard]] constexpr auto next(size_t axis) const noexcept -> Idx {
    assert(axis < DIM);
    Idx res{idx};
    res[axis] += 1;
    return res;
  }

  /**
   * @brief Get the previous index in a given dimension.
   * @param axis Dimension in which the index should be moved.
   * @return Previous index in the given dimension.
   */
  [[nodiscard]] constexpr auto prev(size_t axis) const noexcept -> Idx {
    assert(axis < DIM);
    Idx res{idx};
    res[axis] -= 1;
    return res;
  }

  /**
   * @brief Check whether the index is in the interior.
   * @param size Size of the corresponding space-grid.
   */
  [[nodiscard]] constexpr auto is_interior(const Size<DIM>& size) const noexcept -> bool {
    for (size_t i = 0; i < DIM; ++i) {
      if (idx[i] == 0UL || idx[i] >= size[i]) {
        return false;
      }
    }
    return true;
  }

  /// Default comparison operator
  [[nodiscard]] constexpr auto operator==(const Idx<DIM>& other) const noexcept -> bool = default;
};

/**
 * @class Idx<0>
 * @brief Index into a 0-dimensional space-grid
 *
 * Specialization of Idx<DIM> with DIM=0, basically a real number.
 * Only used for ODEs that do not have a space component.
 */
template <>
struct Idx<0> {
  /// Index into 0-dimensional array, either inside the grid or not.
  bool active;

  /// Advance to next interior point, i.e. to the end state.
  constexpr void advance_interior(const Size<0>& /*size*/) noexcept { active = false; }
  /// Advance to next boundary point, i.e. to the end state.
  constexpr void advance_boundary(const Size<0>& /*size*/) noexcept { active = false; }
  /// Check whether the index is in the interior.
  [[nodiscard]] constexpr auto is_interior(const Size<0>& /*size*/) const noexcept -> bool {
    return active;
  }
  /// Default comparison operator
  [[nodiscard]] constexpr auto operator==(const Idx<0>& other) const noexcept -> bool = default;
};

}  // namespace Heat::Grid

#endif  // HEAT_GRID_INDEX_HPP_
