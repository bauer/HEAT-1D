#ifndef HEAT_TIME_INTEGRATION_HPP_
#define HEAT_TIME_INTEGRATION_HPP_

#include <Eigen/Dense>

#include <AD/ad.hpp>

#include "Concepts.hpp"
#include "Macros.hpp"

namespace Heat::TimeIntegration {

// - Explicit Euler time integration ---------------------------------------------------------------
/**
 * @class ExplicitEuler
 * @brief Time integration using explicit Euler scheme.
 *
 * This is used to intergrate over the time domain.
 * The intergration method implicitly defines the time discretization.
 * The explicit Euler method will create a uniform time discretization with steplength dt.
 */
class ExplicitEuler {
 public:
  /**
   * @brief Do one step of the time integration method.
   * @tparam ActiveFloat Type for the active floating point type, e.g. double or an AD type.
   * @tparam PassiveFloat Type for the passive floating point type, usually double or float.
   * @tparam SpaceGrid Type for the grid that forms the discretization in space.
   * @tparam Rhs Type for the right-hand side of the PDE.
   * @tparam Boundary Type for the boundary condition.
   * @param [out] u_next Value for the next iteration.
   * @param u_curr Current value of the solution.
   * @param dt Time step length.
   * @param rhs Right-hand side of the PDE.
   * @param boundary Boundary condition; unused.
   * @return Length of the time step actually taken, here always equal to dt.
   */
  template <typename ActiveFloat,
            typename PassiveFloat,
            SpaceGrid_c<ActiveFloat, PassiveFloat> SpaceGrid,
            Rhs_c<ActiveFloat, PassiveFloat, SpaceGrid> Rhs,
            Boundary_c<ActiveFloat, SpaceGrid> Boundary>
  [[nodiscard]] constexpr auto
  operator()(SpaceGrid& u_next,
             const SpaceGrid& u_curr,
             PassiveFloat dt,
             const Rhs& rhs,
             const Boundary& /* boundary */) const noexcept -> PassiveFloat {
    assert(u_next.size() == u_curr.size());

    for (auto i = u_curr.begin_interior(); i != u_curr.end_interior();
         i.advance_interior(u_curr.size())) {
      u_next.value_at(i) =
          u_curr.value_at(i) + dt * rhs.template operator()<ActiveFloat, PassiveFloat>(u_curr, i);
    }

    return dt;
  }

  /** @return Name of the integration method. */
  [[nodiscard]] static constexpr auto name() noexcept { return "ExplicitEuler"; }
};

// - Implicit Euler time integration ---------------------------------------------------------------
/**
 * @class ImplicitEuler
 * @brief Time integration using implicit Euler scheme.
 *
 * This is used to intergrate over the time domain.
 * The intergration method implicitly defines the time discretization.
 * The implicit Euler method will create a uniform time discretization with steplength dt.
 */
class ImplicitEuler {
  double m_tol;
  size_t m_max_iter;

 public:
  /**
   * @brief Setup the implicit Euler scheme.
   * @param[optinal] tol Tolerance for the fixed point iteration used in the implicit Euler scheme.
   * @param[optinal] max_iter Maximum number of iterations allowed for the fixed point iteration.
   */
  constexpr ImplicitEuler(double tol = 1e-6, size_t max_iter = 10'000UL) noexcept
      : m_tol(tol),
        m_max_iter(max_iter) {}

  /**
   * @brief Do one step of the time integration method.
   *
   * Internally the implicit Euler method uses a fixed point iteration to solve the implicit scheme.
   * @note For some cases, the fixed point iteration does not converge. If that is the case a
   * warning is printed to stderr.
   *
   * @tparam ActiveFloat Type for the active floating point type, e.g. double or an AD type.
   * @tparam PassiveFloat Type for the passive floating point type, usually double or float.
   * @tparam SpaceGrid Type for the grid that forms the discretization in space.
   * @tparam Rhs Type for the right-hand side of the PDE.
   * @tparam Boundary Type for the boundary condition.
   * @param [out] u_next Value for the next iteration.
   * @param u_curr Current value of the solution.
   * @param dt Time step length.
   * @param rhs Right-hand side of the PDE.
   * @param boundary Boundary condition; unused.
   * @return Length of the time step actually taken, here always equal to dt.
   */
  template <typename ActiveFloat,
            typename PassiveFloat,
            SpaceGrid_c<ActiveFloat, PassiveFloat> SpaceGrid,
            Rhs_c<ActiveFloat, PassiveFloat, SpaceGrid> Rhs,
            Boundary_c<ActiveFloat, SpaceGrid> Boundary>
  [[nodiscard]] constexpr auto operator()(SpaceGrid& u_next,
                                          const SpaceGrid& u_curr,
                                          PassiveFloat dt,
                                          const Rhs& rhs,
                                          const Boundary& boundary) const noexcept -> PassiveFloat {
    assert(u_next.size() == u_curr.size());

    u_next           = u_curr;
    SpaceGrid u_prop = SpaceGrid::like(u_curr, std::numeric_limits<ActiveFloat>::infinity());

    size_t iter = 0UL;
    for (; u_next.diff(u_prop) > m_tol && iter < m_max_iter; ++iter) {
      for (auto i = u_curr.begin_interior(); i != u_curr.end_interior();
           i.advance_interior(u_curr.size())) {
        u_prop.value_at(i) =
            u_curr.value_at(i) + dt * rhs.template operator()<ActiveFloat, PassiveFloat>(u_next, i);
      }
      boundary.template operator()<ActiveFloat>(u_prop);
      u_prop.swap_values(u_next);
    }

#ifndef HEAT_DISABLE_IMPLICIT_EULER_WARNING
    if (std::isnan(u_next.diff(u_prop)) || std::isinf(u_next.diff(u_prop))) {
      HEAT_WARN("Fixed point iteration in implicit Euler solver diverged (||u_next - u_prop|| = "
                << u_next.diff(u_prop) << ")");
    }
    if (iter >= m_max_iter) {
      HEAT_WARN("Fixed point iteration in implicit Euler solver reached maximal iterations ("
                << iter << "), residual is " << u_next.diff(u_prop));
    }
#endif  // HEAT_DISABLE_IMPLICIT_EULER_WARNING

    return dt;
  }

  /** @return Name of the integration method. */
  [[nodiscard]] static constexpr auto name() noexcept { return "ImplicitEuler"; }
};

// - Explicit Runge Kutta Scheme of order 4 --------------------------------------------------------
/**
 * @class RK4
 * @brief Time integration using a fourth order explicit Runge-Kutta scheme.
 *
 * This is used to intergrate over the time domain.
 * The intergration method implicitly defines the time discretization.
 * The Runge-Kutta method will create a uniform time discretization with steplength dt.
 */
class RK4 {
 public:
  /**
   * @brief Do one step of the time integration method.
   *
   * The Runge-Kutta method requires the boundary condition, because it uses intermediate steps for
   * which the boundary values need to be evaluated.
   *
   * @tparam ActiveFloat Type for the active floating point type, e.g. double or an AD type.
   * @tparam PassiveFloat Type for the passive floating point type, usually double or float.
   * @tparam SpaceGrid Type for the grid that forms the discretization in space.
   * @tparam Rhs Type for the right-hand side of the PDE.
   * @tparam Boundary Type for the boundary condition.
   * @param [out] u_next Value for the next iteration.
   * @param u_curr Current value of the solution.
   * @param dt Time step length.
   * @param rhs Right-hand side of the PDE.
   * @param boundary Boundary condition.
   * @return Length of the time step actually taken, here always equal to dt.
   */
  template <typename ActiveFloat,
            typename PassiveFloat,
            SpaceGrid_c<ActiveFloat, PassiveFloat> SpaceGrid,
            Rhs_c<ActiveFloat, PassiveFloat, SpaceGrid> Rhs,
            Boundary_c<ActiveFloat, SpaceGrid> Boundary>
  [[nodiscard]] constexpr auto operator()(SpaceGrid& u_next,
                                          const SpaceGrid& u_curr,
                                          PassiveFloat dt,
                                          const Rhs& rhs,
                                          const Boundary& boundary) const noexcept -> PassiveFloat {
    assert(u_next.size() == u_curr.size());

    SpaceGrid k1 = SpaceGrid::like(u_curr);
    {
      for (auto i = u_curr.begin_interior(); i != u_curr.end_interior();
           i.advance_interior(u_curr.size())) {
        k1.value_at(i) = rhs.template operator()<ActiveFloat, PassiveFloat>(u_curr, i);
      }
      boundary.template operator()<ActiveFloat>(k1);
    }

    SpaceGrid k2 = SpaceGrid::like(u_curr);
    {
      // Temporary abuse u_next to store values
      u_next.values() = u_curr.values() + (dt / static_cast<PassiveFloat>(2)) * k1.values();
      for (auto i = u_curr.begin_interior(); i != u_curr.end_interior();
           i.advance_interior(u_curr.size())) {
        k2.value_at(i) = rhs.template operator()<ActiveFloat, PassiveFloat>(u_next, i);
      }
      boundary.template operator()<ActiveFloat>(k2);
    }

    SpaceGrid k3 = SpaceGrid::like(u_curr);
    {
      // Temporary abuse u_next to store values
      u_next.values() = u_curr.values() + (dt / static_cast<PassiveFloat>(2)) * k2.values();
      for (auto i = u_curr.begin_interior(); i != u_curr.end_interior();
           i.advance_interior(u_curr.size())) {
        k3.value_at(i) = rhs.template operator()<ActiveFloat, PassiveFloat>(u_next, i);
      }
      boundary.template operator()<ActiveFloat>(k3);
    }

    SpaceGrid k4 = SpaceGrid::like(u_curr);
    {
      // Temporary abuse u_next to store values
      u_next.values() = u_curr.values() + dt * k3.values();
      for (auto i = u_curr.begin_interior(); i != u_curr.end_interior();
           i.advance_interior(u_curr.size())) {
        k4.value_at(i) = rhs.template operator()<ActiveFloat, PassiveFloat>(u_next, i);
      }
    }

    u_next.values() =
        u_curr.values() + (dt / static_cast<PassiveFloat>(6)) *
                              (k1.values() + static_cast<PassiveFloat>(2) * k2.values() +
                               static_cast<PassiveFloat>(2) * k3.values() + k4.values());

    return dt;
  }

  /** @return Name of the integration method. */
  [[nodiscard]] static constexpr auto name() noexcept { return "RK4"; }
};

// - Adaptive steplength Runge-Kutta method of order 4(5) ------------------------------------------
/**
 * @class RK45
 * @brief Time integration using an adaptive step length explicit Runge-Kutta scheme of order 4(5).
 *
 * This is used to intergrate over the time domain.
 * The intergration method implicitly defines the time discretization.
 * The Runge-Kutta method will create a potentially non-uniform time discretization.
 */
class RK45 {
  double m_eps;
  size_t m_max_iter;
  double m_small_dt;

 public:
  /**
   * @brief Setup the RK4(5) scheme.
   * @param[optinal] eps Tolerance for the truncation error when comparing the fourth and fifth
   * order method.
   */
  constexpr RK45(double eps = 1e-6, size_t max_iter = 10UL, double small_dt = 1e-8) noexcept
      : m_eps(eps),
        m_max_iter(max_iter),
        m_small_dt(small_dt) {}

  /**
   * @brief Do one step of the time integration method.
   *
   * The Runge-Kutta method requires the boundary condition, because it uses intermediate steps for
   * which the boundary values need to be evaluated.
   * The Runge-Kutta method will update the time step length.
   *
   * @tparam ActiveFloat Type for the active floating point type, e.g. double or an AD type.
   * @tparam PassiveFloat Type for the passive floating point type, usually double or float.
   * @tparam SpaceGrid Type for the grid that forms the discretization in space.
   * @tparam Rhs Type for the right-hand side of the PDE.
   * @tparam Boundary Type for the boundary condition.
   * @param [out] u_next Value for the next iteration.
   * @param u_curr Current value of the solution.
   * @param [in,out] dt Proposed time step length, will update the value to a new proposed length.
   * @param rhs Right-hand side of the PDE.
   * @param boundary Boundary condition.
   * @return Length of the time step actually taken, must **not** be equal to the initial value of
   * dt.
   */
  template <typename ActiveFloat,
            typename PassiveFloat,
            SpaceGrid_c<ActiveFloat, PassiveFloat> SpaceGrid,
            Rhs_c<ActiveFloat, PassiveFloat, SpaceGrid> Rhs,
            Boundary_c<ActiveFloat, SpaceGrid> Boundary>
  [[nodiscard]] constexpr auto operator()(SpaceGrid& u_next,
                                          const SpaceGrid& u_curr,
                                          PassiveFloat& dt,
                                          const Rhs& rhs,
                                          const Boundary& boundary) const noexcept -> PassiveFloat {
    // Reference: https://en.wikipedia.org/wiki/Runge%E2%80%93Kutta%E2%80%93Fehlberg_method
    assert(u_next.size() == u_curr.size());

    //          B(K,L)
    // K  A(K)   L=1    L=2       L=3    L=4    L=5    C(K)   CH(K)   CT(K)
    // 1 	0 		                                       1/9    47/450  1/150
    // 2 	2/9    2/9                                   0      0       0
    // 3 	1/3  	1/12 	  1/4                            9/20   12/25  -3/100
    // 4 	3/4   69/128 -243/128 135/64                16/45   32/225  16/75
    // 5 	1 	 -17/12 	27/4    -27/5   16/15         1/12    1/30    1/20
    // 6 	5/6   65/432 -5/16     13/16   4/27  5/144          6/25   -6/25

    constexpr auto CH1 = static_cast<PassiveFloat>(47) / static_cast<PassiveFloat>(450);
    constexpr auto CH2 = static_cast<PassiveFloat>(0);
    constexpr auto CH3 = static_cast<PassiveFloat>(12) / static_cast<PassiveFloat>(25);
    constexpr auto CH4 = static_cast<PassiveFloat>(32) / static_cast<PassiveFloat>(225);
    constexpr auto CH5 = static_cast<PassiveFloat>(1) / static_cast<PassiveFloat>(30);
    constexpr auto CH6 = static_cast<PassiveFloat>(6) / static_cast<PassiveFloat>(25);

    constexpr auto CT1 = static_cast<PassiveFloat>(1) / static_cast<PassiveFloat>(150);
    constexpr auto CT2 = static_cast<PassiveFloat>(0);
    constexpr auto CT3 = static_cast<PassiveFloat>(-3) / static_cast<PassiveFloat>(100);
    constexpr auto CT4 = static_cast<PassiveFloat>(16) / static_cast<PassiveFloat>(75);
    constexpr auto CT5 = static_cast<PassiveFloat>(1) / static_cast<PassiveFloat>(20);
    constexpr auto CT6 = static_cast<PassiveFloat>(-6) / static_cast<PassiveFloat>(25);

    constexpr auto B21 = static_cast<PassiveFloat>(2) / static_cast<PassiveFloat>(9);

    constexpr auto B31 = static_cast<PassiveFloat>(1) / static_cast<PassiveFloat>(12);
    constexpr auto B32 = static_cast<PassiveFloat>(1) / static_cast<PassiveFloat>(4);

    constexpr auto B41 = static_cast<PassiveFloat>(69) / static_cast<PassiveFloat>(128);
    constexpr auto B42 = static_cast<PassiveFloat>(-243) / static_cast<PassiveFloat>(128);
    constexpr auto B43 = static_cast<PassiveFloat>(135) / static_cast<PassiveFloat>(64);

    constexpr auto B51 = static_cast<PassiveFloat>(-17) / static_cast<PassiveFloat>(12);
    constexpr auto B52 = static_cast<PassiveFloat>(27) / static_cast<PassiveFloat>(4);
    constexpr auto B53 = static_cast<PassiveFloat>(-27) / static_cast<PassiveFloat>(5);
    constexpr auto B54 = static_cast<PassiveFloat>(16) / static_cast<PassiveFloat>(15);

    constexpr auto B61 = static_cast<PassiveFloat>(65) / static_cast<PassiveFloat>(432);
    constexpr auto B62 = static_cast<PassiveFloat>(-5) / static_cast<PassiveFloat>(16);
    constexpr auto B63 = static_cast<PassiveFloat>(13) / static_cast<PassiveFloat>(16);
    constexpr auto B64 = static_cast<PassiveFloat>(4) / static_cast<PassiveFloat>(27);
    constexpr auto B65 = static_cast<PassiveFloat>(5) / static_cast<PassiveFloat>(144);

    SpaceGrid k1 = SpaceGrid::like(u_curr);
    SpaceGrid k2 = SpaceGrid::like(u_curr);
    SpaceGrid k3 = SpaceGrid::like(u_curr);
    SpaceGrid k4 = SpaceGrid::like(u_curr);
    SpaceGrid k5 = SpaceGrid::like(u_curr);
    SpaceGrid k6 = SpaceGrid::like(u_curr);

    auto truncation_error = std::numeric_limits<PassiveFloat>::infinity();
    auto actual_dt        = std::numeric_limits<PassiveFloat>::quiet_NaN();
    size_t iter           = 0UL;
    for (; (truncation_error > static_cast<PassiveFloat>(m_eps)) && (iter < m_max_iter); ++iter) {
      {
        for (auto i = u_curr.begin_interior(); i != u_curr.end_interior();
             i.advance_interior(u_curr.size())) {
          k1.value_at(i) = dt * rhs.template operator()<ActiveFloat, PassiveFloat>(u_curr, i);
        }
        boundary.template operator()<ActiveFloat>(k1);
      }

      {
        for (auto i = u_curr.begin_interior(); i != u_curr.end_interior();
             i.advance_interior(u_curr.size())) {
          // Temporary abuse u_next to store values
          u_next.values() = u_curr.values() + B21 * k1.values();
          k2.value_at(i)  = dt * rhs.template operator()<ActiveFloat, PassiveFloat>(u_next, i);
        }
        boundary.template operator()<ActiveFloat>(k2);
      }

      {
        for (auto i = u_curr.begin_interior(); i != u_curr.end_interior();
             i.advance_interior(u_curr.size())) {
          // Temporary abuse u_next to store values
          u_next.values() = u_curr.values() + B31 * k1.values() + B32 * k2.values();
          k3.value_at(i)  = dt * rhs.template operator()<ActiveFloat, PassiveFloat>(u_next, i);
        }
        boundary.template operator()<ActiveFloat>(k3);
      }

      {
        for (auto i = u_curr.begin_interior(); i != u_curr.end_interior();
             i.advance_interior(u_curr.size())) {
          // Temporary abuse u_next to store values
          u_next.values() =
              u_curr.values() + B41 * k1.values() + B42 * k2.values() + B43 * k3.values();
          k4.value_at(i) = dt * rhs.template operator()<ActiveFloat, PassiveFloat>(u_next, i);
        }
        boundary.template operator()<ActiveFloat>(k4);
      }

      {
        for (auto i = u_curr.begin_interior(); i != u_curr.end_interior();
             i.advance_interior(u_curr.size())) {
          // Temporary abuse u_next to store values
          u_next.values() = u_curr.values() + B51 * k1.values() + B52 * k2.values() +
                            B53 * k3.values() + B54 * k4.values();
          k5.value_at(i) = dt * rhs.template operator()<ActiveFloat, PassiveFloat>(u_next, i);
        }
        boundary.template operator()<ActiveFloat>(k5);
      }

      {
        for (auto i = u_curr.begin_interior(); i != u_curr.end_interior();
             i.advance_interior(u_curr.size())) {
          // Temporary abuse u_next to store values
          u_next.values() = u_curr.values() + B61 * k1.values() + B62 * k2.values() +
                            B63 * k3.values() + B64 * k4.values() + B65 * k5.values();
          k6.value_at(i) = dt * rhs.template operator()<ActiveFloat, PassiveFloat>(u_next, i);
        }
        boundary.template operator()<ActiveFloat>(k6);
      }

      // TODO: Hack, we assume that values returns an eigen-type or a floating point number
      if constexpr (std::is_fundamental_v<std::remove_cvref_t<decltype(u_curr.values())>>) {
        if constexpr (!std::is_same_v<ActiveFloat, PassiveFloat> &&
                      ad::mode<ActiveFloat>::is_ad_type) {
          truncation_error =
              ad::value(std::abs(CT1 * k1.values() + CT2 * k2.values() + CT3 * k3.values() +
                                 CT4 * k4.values() + CT5 * k5.values() + CT6 * k6.values()));
        } else {
          truncation_error = std::abs(CT1 * k1.values() + CT2 * k2.values() + CT3 * k3.values() +
                                      CT4 * k4.values() + CT5 * k5.values() + CT6 * k6.values());
        }
      } else {
        if constexpr (!std::is_same_v<ActiveFloat, PassiveFloat> &&
                      ad::mode<ActiveFloat>::is_ad_type) {
          truncation_error = ad::value((CT1 * k1.values() + CT2 * k2.values() + CT3 * k3.values() +
                                        CT4 * k4.values() + CT5 * k5.values() + CT6 * k6.values())
                                           .norm());
        } else {
          truncation_error = (CT1 * k1.values() + CT2 * k2.values() + CT3 * k3.values() +
                              CT4 * k4.values() + CT5 * k5.values() + CT6 * k6.values())
                                 .norm();
        }
      }

      actual_dt = dt;
      dt        = static_cast<PassiveFloat>(0.9) * dt *
           std::pow(static_cast<PassiveFloat>(m_eps) / truncation_error,
                    static_cast<PassiveFloat>(0.2));

      if (dt < m_small_dt) {
        HEAT_WARN("dt (" << dt << ") has become very small (<" << m_small_dt << ").");
        HEAT_WARN("actual_dt is " << actual_dt << '.');
        HEAT_WARN("Setting dt to " << m_small_dt << '.');
        dt = m_small_dt;
        break;
      }
    }

    if (iter >= m_max_iter) {
      HEAT_WARN("RK4(5) integrator reached maximal number of iterations ("
                << iter << ") but did not converge, truncation error is " << truncation_error
                << " (tolerance is " << m_eps << ")");
    }

    u_next.values() = u_curr.values() + CH1 * k1.values() + CH2 * k2.values() + CH3 * k3.values() +
                      CH4 * k4.values() + CH5 * k5.values() + CH6 * k6.values();

    return actual_dt;
  }

  /** @return Name of the integration method. */
  [[nodiscard]] static constexpr auto name() noexcept { return "RK4(5)"; }
};

}  // namespace Heat::TimeIntegration

#endif  // HEAT_TIME_INTEGRATION_HPP_
