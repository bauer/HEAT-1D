#ifndef HEAT_SPACE_GRID_HELPER_HPP_
#define HEAT_SPACE_GRID_HELPER_HPP_

#include <cstddef>

#include "GridIndex.hpp"

namespace Heat::Grid::Func {

template <typename PassiveFloat>
class Uniform1D {
  size_t m_num_points;
  PassiveFloat m_x_min;
  PassiveFloat m_x_max;
  PassiveFloat m_dx;

 public:
  constexpr Uniform1D(size_t num_points, PassiveFloat x_min, PassiveFloat x_max)
      : m_num_points(num_points),
        m_x_min(x_min),
        m_x_max(x_max),
        m_dx((m_x_max - m_x_min) / static_cast<PassiveFloat>(m_num_points - 1UL)) {}

  [[nodiscard]] constexpr auto operator()(Heat::Grid::Idx<1> idx) const noexcept -> PassiveFloat {
    return m_x_min + dx() * static_cast<PassiveFloat>(idx[0]);
  }

  [[nodiscard]] constexpr auto dx() const noexcept -> PassiveFloat { return m_dx; }
};

}  // namespace Heat::Grid::Func

#endif  // HEAT_SPACE_GRID_HELPER_HPP_
