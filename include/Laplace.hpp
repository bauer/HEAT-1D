#ifndef HEAT_LAPLACE_HPP_
#define HEAT_LAPLACE_HPP_

#include <Eigen/Dense>

#include "Concepts.hpp"

namespace Heat::Laplace {

// - Central finite differences of second order on equidistant grid --------------------------------
/**
 * @class CentralFD
 * @brief Approximate the Laplacian using central finite differences of second order.
 *
 * CentralFD requires the grid to be uniform.
 */
class CentralFD {
 public:
  /**
   * @brief Approximate the Laplacian at a given grid location.
   * @tparam ActiveFloat Type for the active floating point type, e.g. double or an AD type.
   * @tparam PassiveFloat Type for the passive floating point type, usually double or float.
   * @tparam SpaceGrid Type for the grid that forms the discretization in space.
   * @param  grid SpaceGrid containing the space discretization and the value of the function at the
   * respective points.
   * @param  idx Index into the grid at which the Laplacian will be approximated.
   * @return Value for the Laplacian at the index
   */
  template <typename ActiveFloat,
            typename PassiveFloat = ActiveFloat,
            SpaceGrid_c<ActiveFloat, PassiveFloat> SpaceGrid>
  [[nodiscard]] constexpr auto
  operator()(const SpaceGrid& grid,
             const typename SpaceGrid::Index& idx) const noexcept -> ActiveFloat {
    static_assert(SpaceGrid::DIM == 1UL, "Higher dimension than DIM = 1 not implemented yet.");

    assert(idx.is_interior(grid.size()));

    const auto dx = grid.x_at(idx.next(0)) - grid.x_at(idx);

    return (grid.value_at(idx.next(0)) - static_cast<PassiveFloat>(2) * grid.value_at(idx) +
            grid.value_at(idx.prev(0))) /
           (dx * dx);
  }

  /** @return Name of the approximation method.*/
  [[nodiscard]] static constexpr auto name() noexcept { return "CentralFD"; }
};

// - Central finite differences of fourth order on equidistant grid --------------------------------
/**
 * @class CentralFD4
 * @brief Approximate the Laplacian using central finite differences of fourth order.
 *
 * CentralFD4 requires the grid to be uniform.
 */
class CentralFD4 {
 public:
  /**
   * @brief Approximate the Laplacian at a given grid location.
   * @tparam ActiveFloat Type for the active floating point type, e.g. double or an AD type.
   * @tparam PassiveFloat Type for the passive floating point type, usually double or float.
   * @tparam SpaceGrid Type for the grid that forms the discretization in space.
   * @param  grid SpaceGrid containing the space discretization and the value of the function at the
   * respective points.
   * @param  idx Index into the grid at which the Laplacian will be approximated.
   * @return Value for the Laplacian at the index
   * @note The size of the grid must be at least 7, otherwise the Laplacian cannot be approximated
   * with this accuracy.
   */
  template <typename ActiveFloat,
            typename PassiveFloat = ActiveFloat,
            SpaceGrid_c<ActiveFloat, PassiveFloat> SpaceGrid>
  [[nodiscard]] constexpr auto
  operator()(const SpaceGrid& grid,
             const typename SpaceGrid::Index& idx) const noexcept -> ActiveFloat {
    static_assert(SpaceGrid::DIM == 1UL, "Higher dimension than DIM = 1 not implemented yet.");

    // Reference: https://en.wikipedia.org/wiki/Finite_difference_coefficient
    assert(idx.is_interior(grid.size()));

    const auto dx = grid.x_at(idx.next(0)) - grid.x_at(idx);

    // Use central finite differences of fourth order
    if (idx[0] >= 2 && idx[0] <= grid.size()[0] - 3) {
      // (−u(x + 2dx) + 16u(x + dx) − 30u(x) + 16u(x − dx) − u(x − 2dx)) / (12dx^2)
      return (-grid.value_at(idx.next(0).next(0)) +                         //
              static_cast<PassiveFloat>(16) * grid.value_at(idx.next(0)) -  //
              static_cast<PassiveFloat>(30) * grid.value_at(idx) +          //
              static_cast<PassiveFloat>(16) * grid.value_at(idx.prev(0)) -  //
              grid.value_at(idx.prev(0).prev(0))) /
             (static_cast<PassiveFloat>(12) * dx * dx);
    }

    // TODO: There might be a bug here
    // Make sure there are enough points for the forward & backward mode
    assert(grid.size()[0] >= 7);

    // Too far left, fall back to forward finite differences of fourth order
    if (idx[0] < 2) {
      // TODO: This is probabily not the best way to do this...
      return (static_cast<PassiveFloat>(45) * grid.value_at(idx) -                           //
              static_cast<PassiveFloat>(154) * grid.value_at(idx.next(0)) +                  //
              static_cast<PassiveFloat>(214) * grid.value_at(idx.next(0).next(0)) -          //
              static_cast<PassiveFloat>(156) * grid.value_at(idx.next(0).next(0).next(0)) +  //
              static_cast<PassiveFloat>(61) *
                  grid.value_at(idx.next(0).next(0).next(0).next(0)) -  //
              static_cast<PassiveFloat>(10) *
                  grid.value_at(idx.next(0).next(0).next(0).next(0).next(0))) /
             (static_cast<PassiveFloat>(12) * dx * dx);
    }

    // Too far right, fall back to backward finite differences of fourth order
    return (static_cast<PassiveFloat>(45) * grid.value_at(idx) -                                  //
            static_cast<PassiveFloat>(154) * grid.value_at(idx.prev(0)) +                         //
            static_cast<PassiveFloat>(214) * grid.value_at(idx.prev(0).prev(0)) -                 //
            static_cast<PassiveFloat>(156) * grid.value_at(idx.prev(0).prev(0).prev(0)) +         //
            static_cast<PassiveFloat>(61) * grid.value_at(idx.prev(0).prev(0).prev(0).prev(0)) -  //
            static_cast<PassiveFloat>(10) *
                grid.value_at(idx.prev(0).prev(0).prev(0).prev(0).prev(0))) /
           (static_cast<PassiveFloat>(12) * dx * dx);
  }

  /** @return Name of the approximation method.*/
  [[nodiscard]] static constexpr auto name() noexcept { return "CentralFD4"; }
};

// - Central finite differences of second order on non-equidistant grid ----------------------------
/**
 * @class NonUniformCentralFD
 * @brief Approximate the Laplacian using central finite differences of second order.
 *
 * NonUniformCentralFD does not require the grid to be uniform.
 */
class NonUniformCentralFD {
 public:
  /**
   * @brief Approximate the Laplacian at a given grid location.
   * @tparam ActiveFloat Type for the active floating point type, e.g. double or an AD type.
   * @tparam PassiveFloat Type for the passive floating point type, usually double or float.
   * @tparam SpaceGrid Type for the grid that forms the discretization in space.
   * @param  grid SpaceGrid containing the space discretization and the value of the function at the
   * respective points.
   * @param  idx Index into the grid at which the Laplacian will be approximated.
   * @return Value for the Laplacian at the index
   */
  template <typename ActiveFloat,
            typename PassiveFloat = ActiveFloat,
            SpaceGrid_c<ActiveFloat, PassiveFloat> SpaceGrid>
  [[nodiscard]] constexpr auto
  operator()(const SpaceGrid& grid,
             const typename SpaceGrid::Index& idx) const noexcept -> ActiveFloat {
    static_assert(SpaceGrid::DIM == 1, "Higher dimension than DIM=1 is not implemented yet.");

    assert(idx.is_interior(grid.size()));

    const auto dx_prev = grid.x_at(idx) - grid.x_at(idx.prev(0));
    const auto dx_next = grid.x_at(idx.next(0)) - grid.x_at(idx);

    return static_cast<PassiveFloat>(2) *
           (grid.value_at(idx.next(0)) -
            (static_cast<PassiveFloat>(1) + dx_next / dx_prev) * grid.value_at(idx) +
            dx_next / dx_prev * grid.value_at(idx.prev(0))) /
           (dx_prev * dx_next * (1 + dx_next / dx_prev));
  }

  /** @return Name of the approximation method.*/
  [[nodiscard]] static constexpr auto name() noexcept { return "NonUniformCentralFD"; }
};

}  // namespace Heat::Laplace

#endif  // HEAT_LAPLACE_HPP_
