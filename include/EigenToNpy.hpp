#ifndef HEAT_EIGEN_TO_NPY_HPP_
#define HEAT_EIGEN_TO_NPY_HPP_

#include <cstring>
#include <fstream>
#include <string>

#include <Eigen/Dense>

#include "Macros.hpp"
#include "SpaceGrid.hpp"

namespace Heat {

/**
 * @brief Save an Eigen::Matrix to the numpy-compatible `npy` format.
 *
 * The `npy` format saves the matrix data in binary, which makes dumping the data quite fast.
 * Here version 1.0 of the format is used.
 * If the save was not successful, the function returns false and a warning is printed to stderr.
 *
 * @tparam Float Floating point type used in the matrix, must be a C++ floating point.
 * @tparam ROWS Number of rows at compile time, can be Eigen::Dynamic
 * @tparam COLS Number of columns at compile time, can be Eigen::Dynamic
 * @tparam STORAGE_ORDER Storage order of the matrix, row- or column-major
 * @param filename Name of the file in which the matrix is saved, should end with `.npy`
 * @param mat Matrix that should be saved
 * @return Boolean whether the save was successful or not
 */
template <std::floating_point Float, int ROWS, int COLS, int OPTIONS>
[[nodiscard]] auto
save_to_npy(const std::string& filename,
            const Eigen::Matrix<Float, ROWS, COLS, OPTIONS>& mat) noexcept -> bool {
  using namespace std::string_literals;

  std::ofstream out(filename, std::ios::out | std::ios::binary);
  if (!out) {
    HEAT_WARN("Could not open file " << filename
                                     << " for writing in binary: " << std::strerror(errno));
    return false;
  }

  // Write magic string
  constexpr std::streamsize magic_string_len = 6;
  if (!out.write("\x93NUMPY", magic_string_len)) {
    HEAT_WARN("Could not write magic string to " << filename << ": " << std::strerror(errno));
    return false;
  }

  // Write format version, use 1.0
  constexpr std::streamsize version_len = 2;
  if (!out.write("\x01\x00", version_len)) {
    HEAT_WARN("Could not write version number to " << filename << ": " << std::strerror(errno));
    return false;
  }

  // Length of length entry
  constexpr std::streamsize header_len_len = 2;

  // Create header
  // clang-format off
  std::string header =
    "{"s +
        "'descr': '<f"s + std::to_string(sizeof(Float)) + "', "s +
        "'fortran_order': "s + (mat.IsRowMajor ? "False"s : "True"s) + ", "s +
        "'shape': ("s + std::to_string(mat.rows()) + ", "s + std::to_string(mat.cols()) + "), "s +
    "}"s;
  // clang-format on

  // Pad header with spaces s.t. magic string, version, header length and header together are
  // divisible by 64
  for (auto preamble_len = magic_string_len + version_len + header_len_len + header.size() + 1;
       preamble_len % 64 != 0;
       preamble_len = magic_string_len + version_len + header_len_len + header.size() + 1) {
    header.push_back('\x20');
  }
  header.push_back('\n');

  // Write header length
  assert(header.size() <= std::numeric_limits<uint16_t>::max());
  const auto header_len = static_cast<uint16_t>(header.size());
  if (!out.write(reinterpret_cast<const char*>(&header_len), header_len_len)) {  // NOLINT
    HEAT_WARN("Could not write header length to " << filename << ": " << std::strerror(errno));
    return false;
  }

  // Write header
  if (!out.write(header.data(), static_cast<std::streamsize>(header.size()))) {
    HEAT_WARN("Could not write header to " << filename << ": " << std::strerror(errno));
    return false;
  }

  // Write data
  if (!out.write(reinterpret_cast<const char*>(mat.data()),  // NOLINT
                 mat.rows() * mat.cols() * static_cast<std::streamsize>(sizeof(Float)))) {
    HEAT_WARN("Could not write data to " << filename << ": " << std::strerror(errno));
    return false;
  }

  return true;
}

/**
 * @brief Save a Heat::OneD grid to the numpy-compatible `npy` format.
 *
 * Calls to the Eigen::Matrix overload of this function.
 * If the save for either part was not successful, the function returns false and a warning is
 * printed to stderr.
 *
 * @tparam Float Floating point type used in the matrix, must be a C++ floating point.
 * @param x_grid_filename Name of the file in which the grid point positions are saved, should end
 * with `.npy`
 * @param value_filename Name of the file in which the values at the grid points are saved, should
 * end with `.npy`
 * @param grid Grid that should be saved
 * @return Boolean whether the save was successful or not
 */
template <std::floating_point Float>
[[nodiscard]] auto save_to_npy(const std::string& x_grid_filename,
                               const std::string& value_filename,
                               const Grid::OneD<Float, Float>& grid) noexcept -> bool {
  const auto saved_x_grid = save_to_npy(x_grid_filename, grid.x_grid());
  const auto saved_values = save_to_npy(value_filename, grid.values());
  return saved_x_grid && saved_values;
}

}  // namespace Heat

#endif  // HEAT_EIGEN_TO_NPY_HPP_
