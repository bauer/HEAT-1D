#ifndef HEAT_CONCEPTS_HPP_
#define HEAT_CONCEPTS_HPP_

#include <Eigen/Dense>

#include "GridIndex.hpp"

namespace Heat {

// -------------------------------------------------------------------------------------------------
/// @brief Constaint on the grid function that assigns a space position to an index
template <typename Callable, typename Float, size_t DIM>
concept GridFunc_c = requires(Callable grid_func) {
  { grid_func(std::declval<Grid::Idx<DIM>>()) } -> std::same_as<Float>;
};

// -------------------------------------------------------------------------------------------------
/// @brief Constaint on the function for the initial condition that assigns a value to a space
/// position
template <typename Callable, typename ActiveFloat, typename PassiveFloat>
concept U0Func_c = requires(Callable u0_func) {
  { u0_func(std::declval<PassiveFloat>()) } -> std::same_as<ActiveFloat>;
};

// -------------------------------------------------------------------------------------------------
/**
 * @brief Constaint on the right-hand side of the PDE.
 *
 * Function must accept a n-dimensional grid and a index and return an `ActiveFloat`, i.e. the value
 * of the right-hand side at the correnspinding position.
 */
template <typename Callable, typename ActiveFloat, typename PassiveFloat, typename SpaceGrid>
concept Rhs_c = requires(Callable rhs) {
  {
    rhs.template operator()<ActiveFloat, PassiveFloat>(
        std::declval<const SpaceGrid&>(), std::declval<const typename SpaceGrid::Index&>())
  } -> std::same_as<ActiveFloat>;
};

// -------------------------------------------------------------------------------------------------
/**
 * @brief Constaint on the approximation of the gradient sum operator.
 *
 * Function must fullfill the constraints on the right-hand side.
 * Additionally, it must accept an additional function and return a value.
 * Additionally requires a name function.
 */
template <typename Callable,
          typename ActiveFloat,
          typename PassiveFloat,
          typename SpaceGrid,
          typename FUNC>
concept GradientSumOp_c =
    Rhs_c<Callable, ActiveFloat, PassiveFloat, SpaceGrid> && requires(Callable grad_sum_op) {
      {
        grad_sum_op.template operator()<ActiveFloat, PassiveFloat>(
            std::declval<const SpaceGrid&>(),
            std::declval<const typename SpaceGrid::Index&>(),
            std::declval<const FUNC&>())
      } -> std::same_as<ActiveFloat>;

      { Callable::name() } -> std::convertible_to<std::string>;
    };

// -------------------------------------------------------------------------------------------------
/**
 * @brief Constaint on the approximation of the gradient sum operator.
 *
 * Same as the constaint on the right-hand side.
 * Additionally requires a name function.
 */
template <typename Callable, typename ActiveFloat, typename PassiveFloat, typename SpaceGrid>
concept LaplaceOp_c =
    Rhs_c<Callable, ActiveFloat, PassiveFloat, SpaceGrid> && requires(Callable laplace) {
      { Callable::name() } -> std::convertible_to<std::string>;
    };

// -------------------------------------------------------------------------------------------------
/// @brief Constaints on the time integration methods
template <typename Callable,
          typename ActiveFloat,
          typename PassiveFloat,
          typename SpaceGrid,
          typename Rhs,
          typename Boundary>
concept TimeIntegrator_c = requires(Callable integrator) {
  {
    integrator.template operator()<ActiveFloat>(std::declval<SpaceGrid&>(),
                                                std::declval<const SpaceGrid&>(),
                                                std::declval<PassiveFloat&>(),
                                                std::declval<const Rhs&>(),
                                                std::declval<const Boundary&>())
  } -> std::same_as<PassiveFloat>;

  { Callable::name() } -> std::convertible_to<std::string>;
};

// -------------------------------------------------------------------------------------------------
/// @brief Constaints on the boundary condition
template <typename Callable, typename Float, typename SpaceGrid>
concept Boundary_c = requires(Callable boundary) {
  {
    boundary.template operator()<Float, SpaceGrid>(std::declval<SpaceGrid&>())
  } -> std::same_as<void>;

  { Callable::name() } -> std::convertible_to<std::string>;
};

// -------------------------------------------------------------------------------------------------
/// @brief Constaints on the space grid
template <typename Container, typename ActiveFloat, typename PassiveFloat>
concept SpaceGrid_c = requires(Container grid) {
  std::is_same_v<std::remove_cvref_t<decltype(Container::DIM)>, size_t>;
  std::is_same_v<typename Container::Index, Grid::Idx<Container::DIM>>;

  {
    Container::like(std::declval<const Container&>())
  } -> std::same_as<std::remove_cvref_t<Container>>;
  {
    Container::like(std::declval<const Container&>(), std::declval<ActiveFloat>())
  } -> std::same_as<std::remove_cvref_t<Container>>;

  { grid.begin_interior() } -> std::same_as<typename Container::Index>;
  { grid.end_interior() } -> std::same_as<typename Container::Index>;
  { grid.begin_boundary() } -> std::same_as<typename Container::Index>;
  { grid.end_boundary() } -> std::same_as<typename Container::Index>;

  { grid.size() } -> std::same_as<Grid::Size<Container::DIM>>;

  {
    grid.x_at(std::declval<typename Container::Index>())
  } -> std::same_as<
        std::conditional_t<std::is_const_v<Container>, const PassiveFloat&, PassiveFloat&>>;
  {
    grid.value_at(std::declval<typename Container::Index>())
  }
  -> std::same_as<std::conditional_t<std::is_const_v<Container>, const ActiveFloat&, ActiveFloat&>>;

  { grid.swap_values(std::declval<Container&>()) } -> std::same_as<void>;

  { grid.diff(std::declval<const Container&>()) } -> std::same_as<ActiveFloat>;

  // TODO: Make sure those methods exist
  //  values must support:
  //   * multiplication by scalar
  //   * elementwise addition
  // [[nodiscard]] constexpr auto values() noexcept -> Eigen::VectorX<Float>&;
  // [[nodiscard]] constexpr auto values() const noexcept -> const Eigen::VectorX<Float>&;
};

}  // namespace Heat

#endif  // HEAT_CONCEPTS_HPP_
