#ifndef HEAT_SOLVER_HPP_
#define HEAT_SOLVER_HPP_

#include <cassert>

#include <Eigen/Dense>

#include "Concepts.hpp"

namespace Heat {

/**
 * @class Solver
 * @brief Generic solver class
 *
 * The solver is designed to solve any PDE of the form \f$\frac{\partial u}{\partial t} = f[u]\f$,
 * where \f$f[u]\f$ is a functional of \f$u\f$ that can contain any space derivative of u, with some
 * inital and boundary condition.
 * The solver takes the individual elements that discretize the space, time, and boundary and uses
 * them to solve the PDE.
 *
 * @tparam ActiveFloat Type for the active floating point type, e.g. double or an AD type.
 * @tparam PassiveFloat Type for the passive floating point type, usually double or float.
 * @tparam SpaceGrid Type for the space grid, discretizes the space domain and hold the values of
 * the solution.
 * @tparam Rhs Type for the right-hand side of the PDE.
 * @tparam BoundaryCondition Type for the boundary condition, discretizes the boundary.
 * @tparam TimeIntegration Type for integrating the time domain, discretizes the time.
 */
template <
    typename ActiveFloat,
    typename PassiveFloat,
    SpaceGrid_c<ActiveFloat, PassiveFloat> SpaceGrid,
    Rhs_c<ActiveFloat, PassiveFloat, SpaceGrid> Rhs,
    Boundary_c<ActiveFloat, SpaceGrid> BoundaryCondition,
    TimeIntegrator_c<ActiveFloat, PassiveFloat, SpaceGrid, Rhs, BoundaryCondition> TimeIntegration>
class Solver {
  Rhs m_rhs;
  TimeIntegration m_integrator;
  BoundaryCondition m_boundary;
  mutable SpaceGrid m_x_grid;

 public:
  /**
   * @brief Setup the solver for a specific problem definded via `rhs`, `boundary` and `x_grid`
   *
   * `rhs` defines the right-hand side of the PDE, `boundary` defines the boundary condition and
   * `x_grid` defines, besides other things, the initial condition of the problem.
   * @param rhs Right-hand side of the PDE
   * @param integrator Integrator for the time domain
   * @param boundary Boundary condition
   * @param x_grid Space grid containing the initial condition
   */
  constexpr Solver(Rhs rhs,
                   TimeIntegration integrator,
                   BoundaryCondition boundary,
                   SpaceGrid x_grid) noexcept
      : m_rhs(std::move(rhs)),
        m_integrator(std::move(integrator)),
        m_boundary(std::move(boundary)),
        m_x_grid(std::move(x_grid)) {}

  /**
   * @brief Numerically solve the PDE using the provided methods.
   * @param tend Final time for the solution.
   * @param dt Initial value for the time step, can be updated by the time integration method.
   * @return Numerical solution of the PDE at time `tend`.
   * @note The value for `dt` must be chosen, such that the problem is stable. Currently there is
   * not way to determine a proper value for `dt` automatically.
   */
  [[nodiscard]] constexpr auto solve(PassiveFloat tend,
                                     PassiveFloat dt) const noexcept -> SpaceGrid {
    constexpr auto noop = [](const auto&, const auto&, const auto&) constexpr noexcept {
      return static_cast<PassiveFloat>(0);
    };
    PassiveFloat value;
    return solve_and_evaluate(tend, dt, noop, value);
  }

  /**
   * @brief Numerically solve the PDE using the provided methods and apply the provided functional
   * to each timestep.
   * @tparam TargetFunctional Type for the target functional that is evaluated at every iteration of
   * the solver.
   * @tparam FunctionalReturnType Return type of the functional, e.g. float, double, or a vector
   * type.
   * @param tend Final time for the solution.
   * @param dt Initial value for the time step, can be updated by the time integration method.
   * @param F Target functional that that takes the result at a timestep, the delta time, and the
   * previous value of the functional and returns a value.
   * @param [in,out] functional_value Accumulated value of the functional, initialized with provided
   * value.
   * @return Numerical solution of the PDE at time `tend` and accumulated result of the target
   * functional.
   * @note The value for `dt` must be chosen, such that the problem is stable. Currently there is
   * not way to determine a proper value for `dt` automatically.
   */
  template <typename TargetFunctional, typename FunctionalReturnType>
  [[nodiscard]] constexpr auto
  solve_and_evaluate(PassiveFloat tend,
                     PassiveFloat dt,
                     TargetFunctional F,
                     FunctionalReturnType& functional_value) const noexcept -> SpaceGrid {
    assert(tend >= 0 && "tend must be greater or equal to zero");
    assert(dt > 0 && "dt must be greater than zero");

    SpaceGrid& u_curr = m_x_grid;
    SpaceGrid u_next  = SpaceGrid::like(m_x_grid);

    for (PassiveFloat t = 0.0; t < tend;) {  // NOLINT
      if ((tend - t) < dt) {
        dt = tend - t;
      }
      const auto actual_dt =
          m_integrator.template operator()<ActiveFloat>(u_next, u_curr, dt, m_rhs, m_boundary);
      t += actual_dt;
      m_boundary.template operator()<ActiveFloat>(u_next);

      functional_value = F(u_next, actual_dt, functional_value);

      u_curr.swap_values(u_next);
    }

    return u_curr;
  }
};

}  // namespace Heat

#endif  // HEAT_SOLVER_HPP_
