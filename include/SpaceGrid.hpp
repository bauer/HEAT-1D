#ifndef HEAT_SPACE_GRID_HPP_
#define HEAT_SPACE_GRID_HPP_

#include <memory>

#include <Eigen/Dense>

#include <AD/ad.hpp>

#include "Concepts.hpp"
#include "GridIndex.hpp"

namespace Heat::Grid {

// - Zero dimensional grid -------------------------------------------------------------------------
/**
 * @class ZeroD
 * @brief Zero-dimensional grid
 *
 * Used only to solve ODEs that do not have a space component.
 *
 * @tparam ActiveFloat Type for the active floating point type, e.g. double or an AD type.
 * @tparam PassiveFloat Type for the passive floating point type, usually double or float.
 */
template <typename ActiveFloat, typename PassiveFloat>
class ZeroD {
  ActiveFloat m_value{};

 public:
  /// Compile-time known dimension of the grid.
  static constexpr size_t DIM = 0;
  /// Index type for the grid.
  using Index = Idx<DIM>;

  /// Default constructor.
  constexpr ZeroD() noexcept = default;

  /// Value constructor, initializes the grid value to `inital_value`.
  constexpr ZeroD(ActiveFloat inital_value) noexcept
      : m_value(inital_value) {}

  /// Create a different ZeroD object with same dimensions and uninitialized values.
  [[nodiscard]] static constexpr auto like(const ZeroD& /*other*/) noexcept -> ZeroD {
    // return ZeroD{0.0};
    return ZeroD{};
  }
  /// Create a different ZeroD object with same dimensions and values initialized to `value`.
  [[nodiscard]] static constexpr auto like(const ZeroD& /*other*/,
                                           ActiveFloat value) noexcept -> ZeroD {
    return ZeroD(value);
  }

  /// @brief Return an Index to the beginning of the interior, here the only value held in the
  /// object.
  [[nodiscard]] constexpr auto begin_interior() const noexcept -> Index { return Index{true}; }
  /// @brief Return an Index to the end of the interior, as in the C++ standard library end is one
  /// behind the last element.
  [[nodiscard]] constexpr auto end_interior() const noexcept -> Index { return Index{false}; }
  /// @brief Return an Index to the beginning of the boundary, here the boundary does not exist and
  /// therefore the beginning is equal to the end.
  [[nodiscard]] constexpr auto begin_boundary() const noexcept -> Index { return Index{false}; }
  /// @brief Return an Index to the end of the boundary, here the boundary does not exist and
  /// therefore the beginning is equal to the end.
  [[nodiscard]] constexpr auto end_boundary() const noexcept -> Index { return Index{false}; }

  /// @brief Return the size of the grid, returns an empty Size<0>
  [[nodiscard]] constexpr auto size() const noexcept -> Size<0> { return {}; }

  /// @brief Return the grid position at a given index, should not be called as we do not really
  /// have a grid.
  [[nodiscard]] constexpr auto x_at(Index /*idx*/) noexcept -> PassiveFloat& {
    HEAT_TODO("Not implemented yet.");
  }
  /// @brief Return the grid position at a given index, should not be called as we do not really
  /// have a grid.
  [[nodiscard]] constexpr auto x_at(Index /*idx*/) const noexcept -> const PassiveFloat& {
    HEAT_TODO("Not implemented yet.");
  }

  /// @brief Return the value of the grid at a given index.
  [[nodiscard]] constexpr auto value_at(Index idx) noexcept -> ActiveFloat& {
    assert(idx.active);
    return m_value;
  }
  /// @brief Return the value of the grid at a given index.
  [[nodiscard]] constexpr auto value_at(Index idx) const noexcept -> const ActiveFloat& {
    assert(idx.active);
    return m_value;
  }

  /// @brief Swap the values of one ZeroD grid with another.
  constexpr void swap_values(ZeroD& other) noexcept { std::swap(m_value, other.m_value); }

  /// @brief Calculate the difference between two ZeroD grids.
  [[nodiscard]] constexpr auto diff(const ZeroD& other) const noexcept -> ActiveFloat {
    // TODO: Assume either fundamental or AD type, otherwise Eigen Matrix type
    // if constexpr (std::is_fundamental_v<ActiveFloat> || ad::mode<ActiveFloat>::is_ad_type) {
    return std::abs(m_value - other.m_value);
    // } else {
    //   return (m_value - other.m_value).norm();
    // }
  }

  /// @brief Return all values of the grid, here only a scalar value.
  [[nodiscard]] constexpr auto values() noexcept -> ActiveFloat& { return m_value; }
  /// @brief Return all values of the grid, here only a scalar value.
  [[nodiscard]] constexpr auto values() const noexcept -> const ActiveFloat& { return m_value; }
};

// - One dimensional grid --------------------------------------------------------------------------
/**
 * @class OneD
 * @brief One-dimensional space grid
 *
 * Space discretization of a one-dimensional domain.
 * This can represent any uniform or non-uniform grid.
 *
 * @tparam ActiveFloat Type for the active floating point type, e.g. double or an AD type.
 * @tparam PassiveFloat Type for the passive floating point type, usually double or float.
 */
template <typename ActiveFloat, typename PassiveFloat>
class OneD {
  std::shared_ptr<Eigen::VectorX<PassiveFloat>> m_x_grid{};
  Eigen::VectorX<ActiveFloat> m_values{};

  constexpr OneD(std::shared_ptr<Eigen::VectorX<PassiveFloat>> x_grid)
      : m_x_grid(x_grid),
        m_values(Eigen::VectorX<ActiveFloat>(x_grid->rows())) {}

 public:
  /// Compile-time known dimension of the grid.
  static constexpr size_t DIM = 1;
  /// Index type for the grid.
  using Index = Idx<DIM>;

  /**
   * @brief Setup the one-dimensional space grid
   * @tparam X_GRID_FUNC Function that generates the grid positions, it takes a index and returns
   * the corresponding space position.
   * @tparam U0_FUNC Function for the inital value of the problem, takes a space position and
   * returns the corresponding value.
   * @param n Number of grid points.
   * @param x_grid_func Grid position function.
   * @param u0_func Initial condition.
   */
  template <GridFunc_c<PassiveFloat, DIM> X_GRID_FUNC, U0Func_c<ActiveFloat, PassiveFloat> U0_FUNC>
  constexpr OneD(size_t n, const X_GRID_FUNC& x_grid_func, const U0_FUNC& u0_func) noexcept
      : m_x_grid(std::make_shared<Eigen::VectorX<PassiveFloat>>(static_cast<int>(n))),
        m_values(Eigen::VectorX<ActiveFloat>(static_cast<int>(n))) {
    assert(n > 1);
    assert(m_x_grid);

    auto& x_grid = *m_x_grid;
    for (int i = 0; i < static_cast<int>(n); ++i) {
      x_grid(i)   = x_grid_func(Index{static_cast<size_t>(i)});
      m_values(i) = u0_func(x_grid(i));
    }
  }

  /// Create a different ZeroD object with same dimensions and uninitialized values.
  [[nodiscard]] static constexpr auto like(const OneD& other) noexcept -> OneD {
    assert(other.m_x_grid);
    return OneD{other.m_x_grid};
  }

  /// Create a different ZeroD object with same dimensions and initialize the values with `value`.
  [[nodiscard]] static constexpr auto like(const OneD& other, ActiveFloat value) noexcept -> OneD {
    assert(other.m_x_grid);
    OneD res{other.m_x_grid};
    std::fill(std::begin(res.m_values), std::end(res.m_values), value);
    return res;
  }

  // - Get index-range for interior elements -------------------------
  /// @brief Return an Index to the beginning of the interior.
  [[nodiscard]] constexpr auto begin_interior() const noexcept -> Index {
    assert(size()[0] > 2UL);
    return Index{1};
  }
  /// @brief Return an Index to the end of the interior, as in the C++ standard library end is one
  /// behind the last element.
  [[nodiscard]] constexpr auto end_interior() const noexcept -> Index {
    assert(size()[0] > 2UL);
    return Index{size()[0] - 1UL};
  }
  // - Get index-range for interior elements -------------------------

  // - Get index-range for interior elements -------------------------
  /// @brief Return an Index to the beginning of the boundary.
  [[nodiscard]] constexpr auto begin_boundary() const noexcept -> Index { return Index{0}; }
  /// @brief Return an Index to the end of the boundary, as in the C++ standard library end is one
  /// behind the last element.
  [[nodiscard]] constexpr auto end_boundary() const noexcept -> Index { return Index{size()[0]}; }
  // - Get index-range for interior elements -------------------------

  // - Access x-grid at index ----------------------------------------
  /// @brief Get the space position of a grid point with the given index.
  [[nodiscard]] constexpr auto x_at(Index idx) noexcept -> PassiveFloat& {
    assert(m_x_grid);
    assert(idx[0] < static_cast<size_t>(m_x_grid->rows()));
    return (*m_x_grid)(static_cast<Eigen::Index>(idx[0]));
  }

  /// @brief Get the space position of a grid point with the given index.
  [[nodiscard]] constexpr auto x_at(Index idx) const noexcept -> const PassiveFloat& {
    assert(m_x_grid);
    assert(idx[0] < static_cast<size_t>(m_x_grid->rows()));
    return (*m_x_grid)(static_cast<Eigen::Index>(idx[0]));
  }
  // - Access x-grid at index ----------------------------------------

  // - Access value at index -----------------------------------------
  /// @brief Get the value of the grid point at the given index.
  [[nodiscard]] constexpr auto value_at(Index idx) noexcept -> ActiveFloat& {
    assert(idx[0] < static_cast<size_t>(m_values.rows()));
    return m_values(static_cast<Eigen::Index>(idx[0]));
  }

  /// @brief Get the value of the grid point at the given index.
  [[nodiscard]] constexpr auto value_at(Index idx) const noexcept -> const ActiveFloat& {
    assert(idx[0] < static_cast<size_t>(m_values.rows()));
    return m_values(static_cast<Eigen::Index>(idx[0]));
  }
  // - Access value at index -----------------------------------------

  /// @brief Swap the values of two OneD objects
  constexpr void swap_values(OneD& other) noexcept {
    assert(size() == other.size());
    m_values.swap(other.m_values);
  }

  /// @brief Calculate difference between values of two grids
  [[nodiscard]] constexpr auto diff(const OneD& other) const noexcept -> ActiveFloat {
    return (m_values - other.m_values).norm();
  }

  /// @brief Calculate the minimal difference between the position of two grid points
  [[nodiscard]] constexpr auto min_dx() const noexcept -> PassiveFloat {
    assert(m_x_grid);
    const auto& x_grid = *m_x_grid;

    auto min = x_grid(1) - x_grid(0);
    for (int i = 1; i < x_grid.rows() - 1; ++i) {
      min = std::min(min, x_grid(i + 1) - x_grid(i));
    }
    return min;
  }

  /** @brief Evaluate the discretized grid at location x.
   *
   * If the location is not at a grid point, the values of the neighbouring grid points are
   * linearly interpolated. Should x be below the lowest grid point or above the highest, the
   * function is constantly extrapolated.
   *
   * @param x Location at which the grid should be evaluated.
   */
  [[nodiscard]] constexpr auto eval_at(PassiveFloat x) const noexcept -> ActiveFloat {
    assert(m_x_grid);
    const auto& x_grid = *m_x_grid;
    if (x <= x_grid(0)) {
      return m_values(0);
    }
    if (x >= x_grid(m_values.rows() - 1)) {
      return m_values(m_values.rows() - 1);
    }

    const auto first_over = std::upper_bound(std::cbegin(x_grid), std::cend(x_grid), x);
    assert(first_over != std::cbegin(x_grid) && first_over != std::cend(x_grid));
    const auto last_under = std::prev(first_over);

    const auto first_over_idx = std::distance(std::cbegin(x_grid), first_over);
    const auto last_under_idx = std::distance(std::cbegin(x_grid), last_under);

    return (x - *last_under) / (*first_over - *last_under) *
               (m_values(first_over_idx) - m_values(last_under_idx)) +
           m_values(last_under_idx);
  }

  /// @brief Get the number of grid points
  [[nodiscard]] constexpr auto size() const noexcept -> Size<DIM> {
    return Size<DIM>{static_cast<size_t>(m_values.rows())};
  }

  /// @brief Get all grid space positions
  [[nodiscard]] constexpr auto x_grid() noexcept -> Eigen::VectorX<PassiveFloat>& {
    assert(m_x_grid);
    return *m_x_grid;
  }

  /// @brief Get all grid space positions
  [[nodiscard]] constexpr auto x_grid() const noexcept -> const Eigen::VectorX<PassiveFloat>& {
    assert(m_x_grid);
    return *m_x_grid;
  }

  /// @brief Get all grid values
  [[nodiscard]] constexpr auto values() noexcept -> Eigen::VectorX<ActiveFloat>& {
    return m_values;
  }

  /// @brief Get all grid values
  [[nodiscard]] constexpr auto values() const noexcept -> const Eigen::VectorX<ActiveFloat>& {
    return m_values;
  }
};

}  // namespace Heat::Grid

#endif  // HEAT_SPACE_GRID_HPP_
