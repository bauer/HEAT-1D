#ifndef HEAT_BOUNDARY_CONDITION_HPP_
#define HEAT_BOUNDARY_CONDITION_HPP_

#include <Eigen/Dense>

#include "Concepts.hpp"
#include "Macros.hpp"

namespace Heat::Boundary {

// - No-op boundary, does no calculation -----------------------------------------------------------
/**
 * @class Noop
 * @brief Boundary condition that does nothing.
 *
 * Used for calculating ODEs for which no boundary conditions are provided.
 */
template <typename PassiveFloat>
class Noop {
 public:
  /**
   * @brief Does nothing
   */
  template <typename ActiveFloat, SpaceGrid_c<ActiveFloat, PassiveFloat> SpaceGrid>
  constexpr void operator()(SpaceGrid& /*grid*/) const noexcept {}

  /** @return Name of the boundary condition */
  [[nodiscard]] static constexpr auto name() noexcept { return "No-op"; }
};

// - Dirichlet type boundary condition: given value at the boundary --------------------------------
template <typename PassiveFloat>
/**
 * @class Dirichlet
 * @brief Dirichlet type boundary condition.
 *
 * Boundary condition specifies the value of the solution at the boundary.
 *
 * @tparam PassiveFloat Passive floating point type used for the value at the boundary, usually
 * float or double.
 *
 * @warning This boundary condition currently only supports one dimensional problems and must be
 * updated to support higher dimensions.
 */
class Dirichlet {
  PassiveFloat m_value_left;
  PassiveFloat m_value_right;

 public:
  /**
   * @brief Setup the boundary condition.
   * @param value_left Value of the solution at left side boundary.
   * @param value_right Value of the solution at right side boundary.
   */
  constexpr Dirichlet(PassiveFloat value_left, PassiveFloat value_right) noexcept
      : m_value_left(value_left),
        m_value_right(value_right) {}

  /**
   * @brief Update the boundary values of the solution.
   * @tparam ActiveFloat Active floating point type for the solver, e.g. double or an AD type.
   * @tparam SpaceGrid Type for the grid that forms the discretization in space.
   * @param [in,out] grid Value of the solution at a time point, gets updated boundary values.
   */
  template <typename ActiveFloat, SpaceGrid_c<ActiveFloat, PassiveFloat> SpaceGrid>
  constexpr void operator()(SpaceGrid& grid) const noexcept {
    static_assert(SpaceGrid::DIM == 1UL, "Higher dimensions that 1 are not implemented yet.");
    for (auto idx = grid.begin_boundary(); idx != grid.end_boundary();
         idx.advance_boundary(grid.size())) {
      grid.value_at(idx) = (idx[0] == 0UL) ? m_value_left : m_value_right;
    }
  }

  /** @return Name of the boundary condition */
  [[nodiscard]] static constexpr auto name() noexcept { return "Dirichlet"; }
};

// - Von Neumann type boundary condition: given derivative w.r.t. space at the boundary ------------
/**
 * @class Neumann
 * @brief Neumann type boundary condition.
 *
 * Boundary condition specifies the value of the space-derivative at the boundary.
 *
 * @tparam PassiveFloat Passive floating point type used for the value at the boundary, usually
 * float or double.
 * @tparam ORDER Order of the fitinte differences approximation of the space-derivative at the
 * boundary.
 *
 * @note For ORDER=1 the grid must have at least size 2 and for ORDER=2 the grid must at least have
 * size 3.
 * @warning This boundary condition currently only supports one dimensional problems and must be
 * updated to support higher dimensions.
 */
template <typename PassiveFloat, size_t ORDER = 1UL>
class Neumann {
  static_assert(ORDER >= 1UL,
                "Order for finite difference approximation must be greater than zero.");
  PassiveFloat m_value_left;
  PassiveFloat m_value_right;

 public:
  /**
   * @brief Setup the boundary condition.
   * @param value_left Value of the space-derivative at left side boundary.
   * @param value_right Value of the space-derivative at right side boundary.
   */
  constexpr Neumann(PassiveFloat value_left, PassiveFloat value_right) noexcept
      : m_value_left(value_left),
        m_value_right(value_right) {}

  /**
   * @brief Update the boundary values of the solution.
   * @tparam ActiveFloat Active floating point type for the solver, e.g. double or an AD type.
   * @tparam SpaceGrid Type for the grid that forms the discretization in space.
   * @param [in,out] grid Value of the solution at a time point, gets updated boundary values.
   */
  template <typename ActiveFloat, SpaceGrid_c<ActiveFloat, PassiveFloat> SpaceGrid>
  constexpr void operator()(SpaceGrid& grid) const noexcept {
    static_assert(SpaceGrid::DIM == 1UL, "Higher dimensions that 1 are not implemented yet.");
    if constexpr (ORDER == 1UL) {
      assert(grid.size()[0] >= 2);

      for (auto idx = grid.begin_boundary(); idx != grid.end_boundary();
           idx.advance_boundary(grid.size())) {
        // Left side
        if (idx[0] == 0UL) {
          //    (u_next(1) - u_next(0)) / dx = value_left
          // => u_next(0) = u_next(1) - value_left * dx
          const auto dx      = grid.x_at(idx.next(0)) - grid.x_at(idx);
          grid.value_at(idx) = grid.value_at(idx.next(0)) - m_value_left * dx;
        }
        // Right side
        else {
          //    (u_next(n-1) - u_next(n-2)) / dx = value_right
          // => u_next(n-1) = u_next(n-2) + value_right * dx
          const auto dx      = grid.x_at(idx) - grid.x_at(idx.prev(0));
          grid.value_at(idx) = grid.value_at(idx.prev(0)) + m_value_right * dx;
        }
      }
    } else if constexpr (ORDER == 2UL) {
      // TODO: Assumes uniform grid
      assert(grid.size()[0] >= 3);

      for (auto idx = grid.begin_boundary(); idx != grid.end_boundary();
           idx.advance_boundary(grid.size())) {
        // Left side
        if (idx[0] == 0UL) {
          //    (-3*u_next(0) + 4*u_next(1) - u_next(2)) / (2*dx) = value_left
          // => u_next(0) = (1/3) * (4*u_next(1) - u_next(2) - 2*dx*value_left)
          const auto dx      = grid.x_at(idx.next(0)) - grid.x_at(idx);
          grid.value_at(idx) = (static_cast<PassiveFloat>(4) * grid.value_at(idx.next(0)) -
                                grid.value_at(idx.next(0).next(0)) -
                                static_cast<PassiveFloat>(2) * dx * m_value_left) /
                               static_cast<PassiveFloat>(3);
        }
        // Right side
        else {
          //    (3*u_next(n-1) - 4*u_next(n-2) + u_next(n-3)) / (2*dx) = value_right
          // => u_next(n-1) = (1/3) * (4*u_next(n-2) - u_next(n-3) - 2*dx*value_right)
          const auto dx      = grid.x_at(idx) - grid.x_at(idx.prev(0));
          grid.value_at(idx) = (static_cast<PassiveFloat>(4) * grid.value_at(idx.prev(0)) -
                                grid.value_at(idx.prev(0).prev(0)) -
                                static_cast<PassiveFloat>(2) * dx * m_value_right) /
                               static_cast<PassiveFloat>(3);
        }
      }
    } else {
      HEAT_PANIC("Von Neumann boundary conditions using finite differences of order "
                 << ORDER << " are not implemented yet.");
    }
  }

  /** @return Name of the boundary condition */
  [[nodiscard]] static constexpr auto name() noexcept {
    return "Neumann (" + std::to_string(ORDER) + ")";
  }
};

}  // namespace Heat::Boundary

#endif  // HEAT_BOUNDARY_CONDITION_HPP_
