#ifndef HEAT_RHS_HPP_
#define HEAT_RHS_HPP_

#include "Concepts.hpp"
#include "GradientSum.hpp"
#include "Laplace.hpp"

namespace Heat::Rhs {

// - Right hand side for the heat equation ---------------------------------------------------------
/**
 * @class HeatEq
 * @brief Right-hand side of the heat equation
 *
 * The right-hand side of the heat equation is a constant diffusion coefficient a times the
 * Laplacian of the function:
 *  \f$\frac{\partial u}{\partial t} = f[u]\f$ with
 *  \f$f[u] = a \cdot \sum_{i=1}^{d} \frac{\partial^2 u}{\partial
 * x_i^2}\f$. The numerical approximation of the Laplacian must be provided to this class.
 *
 * @tparam LaplaceOp Type for the numerical approximation of the Laplacian
 */
template <typename Float, typename LaplaceOp>
class HeatEq {
  Float m_a{};
  LaplaceOp m_laplace{};

 public:
  /**
   * @brief Setup the right-hand side for the heat equation
   * @param a Diffusion coefficient
   * @param laplace Numerical approximation of the Laplacian
   */
  constexpr HeatEq(Float a, LaplaceOp laplace) noexcept
      : m_a(a),
        m_laplace(std::move(laplace)) {}

  /**
   * @brief Evaluate the right-hand side of the heat equation at a grid point.
   *
   * @tparam ActiveFloat Type for the active floating point type, e.g. double or an AD type.
   * @tparam PassiveFloat Type for the passive floating point type, usually double or float.
   * @tparam SpaceGrid Type for the grid that forms the discretization in space.
   * @param  grid SpaceGrid containing the space discretization and the value of the function at the
   * respective points.
   * @param  idx Index into the grid at which the Laplacian will be approximated.
   * @return Value of the right-hand side at the grid position
   */
  template <typename ActiveFloat,
            typename PassiveFloat,
            Heat::SpaceGrid_c<ActiveFloat, PassiveFloat> SpaceGrid>
  [[nodiscard]] constexpr auto
  operator()(const SpaceGrid& grid,
             const typename SpaceGrid::Index& idx) const noexcept -> ActiveFloat {
    static_assert(Heat::LaplaceOp_c<LaplaceOp, ActiveFloat, PassiveFloat, SpaceGrid>,
                  "The provided LaplaceOp class does not fullfill the contraints for a numerical "
                  "laplace operator.");
    static_assert(std::is_same_v<std::remove_cvref_t<Float>, std::remove_cvref_t<ActiveFloat>> ||
                      std::is_same_v<std::remove_cvref_t<Float>, std::remove_cvref_t<PassiveFloat>>,
                  "Expected `Float` to be the same type as `ActiveFloat` or `PassiveFloat`.");
    return m_a * m_laplace.template operator()<ActiveFloat, PassiveFloat>(grid, idx);
  }
};

// - Right hand side for Burgers equation ----------------------------------------------------------
/**
 * @class BurgersEq
 * @brief Right-hand side of the burgers equation
 *
 * The right-hand side of the burgers equation is \f$f[u] = \sum_{i=1}^{d} \frac{\partial}{\partial
 * x_i} u^2 \f$ with the PDE \f$\frac{\partial u}{\partial t} = f[u]\f$.
 * The numerical approximation of the sum over the gradient must be provided to this class.
 *
 * @tparam GradientSumOp Type for the numerical approximation of the sum over the gradient for a
 * function \f$g(u)\f$
 */
template <typename GradientSumOp>
class BurgersEq {
  GradientSumOp m_grad_sum;

 public:
  /**
   * @brief Setup the right-hand side for burgers equation
   * @param grad_sum Numerical approximation of the sum over the gradient for a
   * function \f$g(u)\f$
   */
  constexpr BurgersEq(GradientSumOp grad_sum) noexcept
      : m_grad_sum(grad_sum) {}

  /**
   * @brief Evaluate the right-hand side of burgers equation at a grid point.
   *
   * @tparam ActiveFloat Type for the active floating point type, e.g. double or an AD type.
   * @tparam PassiveFloat Type for the passive floating point type, usually double or float.
   * @tparam SpaceGrid Type for the grid that forms the discretization in space.
   * @param  grid SpaceGrid containing the space discretization and the value of the function at the
   * respective points.
   * @param  idx Index into the grid at which the Laplacian will be approximated.
   * @return Value of the right-hand side at the grid position
   */
  template <typename ActiveFloat,
            typename PassiveFloat,
            Heat::SpaceGrid_c<ActiveFloat, PassiveFloat> SpaceGrid>
  [[nodiscard]] constexpr auto
  operator()(const SpaceGrid& grid,
             const typename SpaceGrid::Index& idx) const noexcept -> ActiveFloat {
    constexpr auto f = [](ActiveFloat u) constexpr noexcept -> ActiveFloat {
      return static_cast<ActiveFloat>(0.5) * u * u;
    };

    static_assert(
        Heat::GradientSumOp_c<GradientSumOp, ActiveFloat, PassiveFloat, SpaceGrid, decltype(f)>,
        "The provided GradientSumOp class does not fullfill the contraints for a numerical "
        "GradientSum operator.");

    return static_cast<PassiveFloat>(-1) *
           m_grad_sum.template operator()<ActiveFloat, PassiveFloat>(grid, idx, f);
  }
};

}  // namespace Heat::Rhs

#endif  // HEAT_RHS_HPP_
