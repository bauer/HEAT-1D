#ifndef HEAT_BASELINE_SOLVER_HPP_
#define HEAT_BASELINE_SOLVER_HPP_

#include <fstream>
#include <optional>
#include <type_traits>

#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/SparseLU>

#include <AD/ad.hpp>

#include "Macros.hpp"

namespace Heat {

// - Save vector in binary to a file ---------------------------------------------------------------
template <typename Float>
  requires std::is_floating_point_v<Float>
[[nodiscard]] constexpr auto save_vector_binary(const Eigen::VectorX<Float>& v,
                                                std::optional<std::ofstream>& out) noexcept
    -> bool {
  if (!out.has_value()) {
    return true;
  }

  if (!out->write(reinterpret_cast<const char*>(v.data()),  // NOLINT
                  v.rows() * static_cast<Eigen::Index>(sizeof(Float)))) {
    HEAT_WARN("Could not save vector to file.");
    return false;
  }

  return true;
}

template <typename ADType>
  requires ad::mode<ADType>::is_ad_type
[[nodiscard]] constexpr auto save_vector_binary(const Eigen::VectorX<ADType>& v,
                                                std::optional<std::ofstream>& out) noexcept
    -> bool {
  if (!out.has_value()) {
    return true;
  }

  static_cast<void>(v);
  HEAT_TODO("Saving vector of ad type to file is not implemented yet.");
  return false;
}

// - Solver using Explicit Euler Method in a matrix free version -----------------------------------
template <typename AT, typename PT>
[[nodiscard]] constexpr auto
solve_explicit_matrix_free(const Eigen::VectorX<AT>& u0,
                           AT a,
                           PT tend,
                           PT dx,
                           PT dt,
                           std::optional<std::ofstream>& u_out) noexcept
    -> std::optional<Eigen::VectorX<AT>> {
  const auto n = u0.rows();
  assert(n >= 2);
  Eigen::VectorX<AT> u_curr = u0;
  Eigen::VectorX<AT> u_next(n);

  if (!save_vector_binary(u_curr, u_out)) {
    return std::nullopt;
  }

  for (PT t = 0.0; t < tend; t += dt) {  // NOLINT
    for (Eigen::Index i = 1; i < n - 1; ++i) {
      u_next(i) =
          u_curr(i) + dt * a * (u_curr(i + 1) - 2.0 * u_curr(i) + u_curr(i - 1)) / (dx * dx);
    }
    u_next(0)     = u_curr(0);
    u_next(n - 1) = u_curr(n - 1);

    if (!save_vector_binary(u_curr, u_out)) {
      return std::nullopt;
    }

    u_curr.swap(u_next);
  }

  return u_curr;
}

// - Solver using Explicit Euler Method ------------------------------------------------------------
template <typename AT, typename PT>
[[nodiscard]] constexpr auto solve_explicit(const Eigen::VectorX<AT>& u0,
                                            AT a,
                                            PT tend,
                                            PT dx,
                                            PT dt,
                                            std::optional<std::ofstream>& u_out) noexcept
    -> std::optional<Eigen::VectorX<AT>> {
  const auto n = u0.rows();
  assert(n >= 2);
  Eigen::VectorX<AT> u_curr = u0;
  Eigen::VectorX<AT> u_next(n);

  Eigen::SparseMatrix<AT> A(n, n);
  {
    std::vector<Eigen::Triplet<AT>> triplets(static_cast<size_t>(n - 2) * 3UL);
    const auto factor = a * dt / (dx * dx);
    for (int i = 1; i < n - 1; ++i) {
      triplets.at(3UL * static_cast<size_t>(i - 1) + 0UL) = {i, i, -2.0 * factor};
      triplets.at(3UL * static_cast<size_t>(i - 1) + 1UL) = {i, i - 1, factor};
      triplets.at(3UL * static_cast<size_t>(i - 1) + 2UL) = {i, i + 1, factor};
    }
    A.setFromTriplets(std::cbegin(triplets), std::cend(triplets));
  }

  if (!save_vector_binary(u_curr, u_out)) {
    return std::nullopt;
  }

  for (PT t = 0.0; t < tend; t += dt) {  // NOLINT
    u_next = u_curr + A * u_curr;

    if (!save_vector_binary(u_curr, u_out)) {
      return std::nullopt;
    }

    u_curr.swap(u_next);
  }

  return u_curr;
}

// - Solver using Implicit Euler Method ------------------------------------------------------------
template <typename AT, typename PT>
[[nodiscard]] constexpr auto solve_implicit(const Eigen::VectorX<AT>& u0,
                                            AT a,
                                            PT tend,
                                            PT dx,
                                            PT dt,
                                            std::optional<std::ofstream>& u_out) noexcept
    -> std::optional<Eigen::VectorX<AT>> {
  const auto n = u0.rows();
  assert(n >= 2);
  Eigen::VectorX<AT> u_curr = u0;
  Eigen::VectorX<AT> u_next(n);

  Eigen::SparseMatrix<AT> I_minus_A(n, n);
  {
    std::vector<Eigen::Triplet<AT>> triplets(static_cast<size_t>(n - 2) * 3UL + 2UL);
    const auto factor = a * dt / (dx * dx);
    for (int i = 1; i < n - 1; ++i) {
      triplets.at(3UL * static_cast<size_t>(i - 1) + 0UL) = {i, i, 1.0 + 2.0 * factor};
      triplets.at(3UL * static_cast<size_t>(i - 1) + 1UL) = {i, i - 1, -factor};
      triplets.at(3UL * static_cast<size_t>(i - 1) + 2UL) = {i, i + 1, -factor};
    }
    triplets.at(triplets.size() - 2UL) = {0, 0, 1.0};
    triplets.at(triplets.size() - 1UL) = {static_cast<int>(n - 1), static_cast<int>(n - 1), 1.0};
    I_minus_A.setFromTriplets(std::cbegin(triplets), std::cend(triplets));
  }

  Eigen::SparseLU<Eigen::SparseMatrix<AT>> lu(I_minus_A);

  if (!save_vector_binary(u_curr, u_out)) {
    return std::nullopt;
  }

  for (PT t = 0.0; t < tend; t += dt) {  // NOLINT
    u_next = lu.solve(u_curr);

    if (!save_vector_binary(u_curr, u_out)) {
      return std::nullopt;
    }

    u_curr.swap(u_next);
  }

  return u_curr;
}

// - Different mode with which the heat equation can be solved -------------------------------------
enum class SolverMode { ExplicitMatrixFree, Explicit, Implicit };

auto operator<<(std::ostream& out, SolverMode mode) noexcept -> std::ostream& {
  switch (mode) {
    case SolverMode::ExplicitMatrixFree:
      out << "ExplicitMatrixFree";
      break;
    case SolverMode::Explicit:
      out << "Explicit";
      break;
    case SolverMode::Implicit:
      out << "Implicit";
      break;
    default:
      HEAT_PANIC("Invalid solver mode " << static_cast<int>(mode));
  }
  return out;
}

// - Solve heat equation with the specified mode ---------------------------------------------------
template <typename AT, typename PT>
[[nodiscard]] constexpr auto solve(const Eigen::VectorX<AT>& u0,
                                   AT a,
                                   PT tend,
                                   PT dx,
                                   PT dt,
                                   std::optional<std::ofstream>& u_out,
                                   SolverMode mode) noexcept -> std::optional<Eigen::VectorX<AT>> {
  switch (mode) {
    case SolverMode::ExplicitMatrixFree:
      return solve_explicit_matrix_free(u0, a, tend, dx, dt, u_out);
    case SolverMode::Explicit:
      return solve_explicit(u0, a, tend, dx, dt, u_out);
    case SolverMode::Implicit:
      return solve_implicit(u0, a, tend, dx, dt, u_out);
    default:
      HEAT_PANIC("Invalid solver mode " << static_cast<int>(mode));
  }
}

// - Solve heat equation with the specified mode, do not save intermediate results -----------------
template <typename AT, typename PT>
[[nodiscard]] auto
solve(const Eigen::VectorX<AT>& u0, AT a, PT tend, PT dx, PT dt, SolverMode mode) noexcept
    -> std::optional<Eigen::VectorX<AT>> {
  std::optional<std::ofstream> nullstream = std::nullopt;
  return solve(u0, a, tend, dx, dt, nullstream, mode);
}

}  // namespace Heat

#endif  // HEAT_BASELINE_SOLVER_HPP_
