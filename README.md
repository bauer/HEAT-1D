# Generic solver for the 1 dimensional heat equation

## Quickstart

```console
$ cmake -Bbuild
$ cd bin
$ make
$ ./run_heat_1d
$ ./run_burgers_1d
$
$ ./test_ad
$ ./test_ode
$ ./test_heat
```

## Solver architecture

The solver is designed to solve (partial) differential equations of the form
$$
\frac{\partial u}{\partial t} = f[u].
$$
Here the functional $f[u]$ can contain any derivative of $u$ with respect to the space domain $x$.
For example $f$ for the one dimensional heat equation is $f[u] = a \frac{\partial^2 u}{\partial x^2}$ and for Burgers equation $f[u] = -\frac{\partial}{\partial x} (\tfrac{1}{2} u^2)$.

The equation is the solved by first discretizing the right-hand-side $f$ in the space domain, transforming the PDE to an ODE on the time domain.

This resulting ODE is then solved using a numerical Integrator, e.g. a Runge-Kutta scheme.

The initial condition to the problem is passed via the space grid, i.e. the initial value on the grid points is the initial condition.

Additionally to the initial condition boundary conditions are passed to the solver.

The library provides some useful numerical approximations of commonly used operators, e.g. the laplace operator.

### Boundary conditions

- Dirichlet type boundary conditions (`Heat::Boundary::Dirichlet`)
- Von Neumann type boundary conditions using finite differences of order n=1,2 (`Heat::Boundary::Neumann<n>`)

### Time integration

- Explicit Euler (`Heat::TimeIntegration::ExplicitEuler`)
- Implicit Euler (`Heat::TimeIntegration::ImplicitEuler`)
- Explicit Runge Kutta Scheme of order 4 (`Heat::TimeIntegration::RK4`)
- Adaptive step-length explicit Runge Kutta Scheme of order 4(5) (`Heat::TimeIntegration::RK45`)

### Numerical approximation of the laplace operator

$$ y \approx \Delta_x u $$

- Central finite differences of second order on equidistant x-grid (`Heat::Laplace::CentralFD`)
- Central finite differences on fourth order on equidistant x-grid (`Heat::Laplace::CentralFD4`)
- Central finite differences of second order on non-equidistant x-grid (`Heat::Laplace::NonUniformCentralFD`)

### Numerical approximations of the sum of the gradient entries

$$
y \approx \sum_{i = 1}^{n} \frac{\partial g(u)}{\partial x_i} \\
g: \mathbb{R} \rightarrow{} \mathbb{R} 
$$


- Central finite differences of second order on equidistant x-grid (`Heat::GradientSum::ForwardFD`)
- Forwards finite differences of first order on non-equidistant x-grid (`Heat::GradientSum::BackwardFD`)
- Backwards finite differences of first order on non-equidistant x-grid (`Heat::GradientSum::CentralFD`)

## Analysis

The output of the simulation is written to a `.npy`-file.
The analysis of the output is done in python, the corresponding notebooks are found in the `notebooks` directory.

## Third party dependencies

- [Eigen](https://eigen.tuxfamily.org/index.php?title=Main_Page)
- [ad.hpp](https://www.stce.rwth-aachen.de/)
- [PerlinNoise.hpp](https://github.com/Reputeless/PerlinNoise)
